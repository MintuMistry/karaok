<?php  

namespace App\Models;

use CodeIgniter\Model;

class Content extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('page_content');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read()
	{	
		$this->builder->orderBy('content_id', 'DESC');
		return $this->builder->get()->getResultArray();
	}

	public function crud_update($data, $content_id)
	{	
		$this->builder->where("content_id",$content_id);
		$this->builder->update($data);
	}

	public function crud_delete($content_id)
	{	
		$this->builder->where('content_id', $content_id);
		$this->builder->delete();
	}
}


?>