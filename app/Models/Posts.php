<?php  

namespace App\Models;

use CodeIgniter\Model;

class Posts extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('posts_details');
    }
	
	function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}
	
	function crud_read($post_id = '')
	{	
		if($post_id > 0){
			$this->builder->where("post_id",$post_id);
			return $this->builder->get()->getResultArray();
		}
		else
			$this->builder->orderBy("post_id","DESC");
			return $this->builder->get()->getResultArray();		
	}

	function crud_update($data, $post_id)
	{	
		$this->builder->where("post_id",$post_id);
		$this->builder->update($data);
	}

	public function crud_delete($post_id)
	{	
		$this->builder->where('post_id', $post_id);
		$this->builder->delete();
	}

}


?>