<?php  

namespace App\Models;

use CodeIgniter\Model;

class Songs extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('songs_detail');
   		$this->builder1 =  $this->db->table('user_videos');
   		$this->builder2 =  $this->db->table('post_report');
   		$this->builder3 =  $this->db->table('post_comments');
   		$this->builder4 =  $this->db->table('post_likes');
   		$this->builder5 =  $this->db->table('music_category');
   		$this->builder6 =  $this->db->table('my_favorite');
   		$this->builder7 =  $this->db->table('user_following');
   		$this->builder8 =  $this->db->table('comment_reply');
   		$this->builder9 =  $this->db->table('comment_likes');
   		$this->builder10 =  $this->db->table('comment_reply_likes');
   		$this->builder11 =  $this->db->table('user_music_choice');
   		$this->builder12 =  $this->db->table('sound_effets');
    }
	
	public function crud_read_video_by_cat($music_cat_id)
	{
		$this->builder1->where("music_cat_id",$music_cat_id);
		$this->builder1->orderBy('total_likes', 'DESC');
		$this->builder1->where("video_status",0);
		$this->builder1->LIMIT(20,0);
		return $this->builder1->get()->getResultArray();
	}
	
	
	public function crud_read_sound_effects($sound_id ='')
	{
		if($sound_id !='')
		{
			$this->builder12->where("sound_id",$sound_id);
			$this->builder12->orderBy('sound_id', 'DESC');
			return $this->builder12->get()->getResultArray();
		}else{
			$this->builder12->orderBy('sound_id', 'DESC');
			return $this->builder12->get()->getResultArray();
		}
	}
	
	public function crud_read_user_likes($videoid)
	{	
		$today = date('Y-m-d');
		$seventh_date = date('Y-m-d', strtotime('-7 days'));
		
		/* return $this->db->query("SELECT * FROM post_likes WHERE video_id ='$videoid' created_on >='$seventh_date' AND created_on <='$today'")->getResultArray(); */
			
		$this->builder1->where("video_id",$videoid);
		$this->builder1->where("video_status",0);
		$this->builder1->where("created_on >=",$seventh_date);
		$this->builder1->where("created_on <=",$today);
		return $this->builder1->get()->getResultArray();
		
	}
	
	public function crud_read_recomended_video($min_range ='',$max_range ='')
	{	
		$today = date('Y-m-d H:i:s');
		$seventh_date = date('Y-m-d H:i:s', strtotime('-7 days'));
		if($min_range !='' && $max_range !='')
		{
			return $this->db->query("SELECT *,user_videos.video_id as videoid FROM user_videos LEFT JOIN post_likes on user_videos.video_id = post_likes.video_id WHERE
			user_videos.video_status = 0 AND post_likes.created_on >='$seventh_date' AND post_likes.created_on <='$today' GROUP BY user_videos.video_id ORDER BY user_videos.total_likes DESC LIMIT $min_range,$max_range")->getResultArray();
			
			//$this->builder1->where("video_id <",$lastid);
			//$this->builder1->where("user_id",$user_id);
			//$this->builder1->wherein("music_cat_id",$userchoice);
			//$this->builder1->orderBy('total_likes', 'DESC');
			//$this->builder1->LIMIT($max_range,$min_range);
			//return $this->builder1->get()->getResultArray();
		}else{
			return $this->db->query("SELECT * FROM user_videos LEFT JOIN post_likes on user_videos.video_id = post_likes.video_id WHERE user_videos.video_status = 0 AND post_likes.created_on >='$seventh_date' AND post_likes.created_on <='$today' GROUP BY user_videos.video_id ORDER BY user_videos.total_likes DESC LIMIT 0,5")->getResultArray();
			/* $this->builder1->where("user_id",$user_id);
			//$this->builder1->wherein("music_cat_id",$userchoice);
			$this->builder1->orderBy('total_likes', 'DESC');
			$this->builder1->LIMIT(5,0);
			return $this->builder1->get()->getResultArray(); */
		}
	}
	
	public function crud_read_popular_video($min_range ='',$max_range ='')
	{
		if($min_range !='' && $max_range !='')
		{
			//$this->builder1->where("video_id <",$lastid);
			$this->builder1->orderBy('total_likes', 'DESC');
			$this->builder1->where("video_status",0);
			$this->builder1->groupBy('video_id');
			$this->builder1->LIMIT($max_range,$min_range);
			return $this->builder1->get()->getResultArray();
		}else{
			$this->builder1->where("video_status",0);
			$this->builder1->groupBy('video_id');
			$this->builder1->orderBy('total_likes', 'DESC');
			$this->builder1->LIMIT(5,0);
			return $this->builder1->get()->getResultArray();
		}
	}
	
	public function crud_read_trending_video($min_range ='',$max_range ='')
	{
		if($min_range !='' && $max_range !='')
		{
			//$this->builder1->where("video_id <",$lastid);
			$this->builder1->where("video_status",0);
			$this->builder1->groupBy('video_id');
			$this->builder1->orderBy('total_sing', 'DESC');
			$this->builder1->LIMIT($max_range,$min_range);
			return $this->builder1->get()->getResultArray();
		}else{
			$this->builder1->where("video_status",0);
			$this->builder1->groupBy('video_id');
			$this->builder1->orderBy('total_sing', 'DESC');
			$this->builder1->LIMIT(5,0);
			return $this->builder1->get()->getResultArray();
		}
	}
	
	public function crud_read_top_artist_video($userid)
	{
		$this->builder1->where("user_id",$userid);
		$this->builder1->where("video_status",0);
		$this->builder1->orderBy('total_likes', 'DESC');
		$this->builder1->LIMIT(1,0);
		return $this->builder1->get()->getResultArray();
	}
	
	public function crud_read_top_singer_video($userid)
	{
		$this->builder1->where("user_id",$userid);
		$this->builder1->where("video_status",0);
		$this->builder1->orderBy('total_likes', 'DESC');
		$this->builder1->LIMIT(1,0);
		return $this->builder1->get()->getResultArray();
	}
	
	public function crud_filter_song($txt)
	{	
		return $this->db->query("SELECT  * FROM user_videos WHERE video_status = 0 AND song_name like'%$txt%' OR artist_name like '%$txt%' ")->getResultArray();
	}
	
	public function curd_read_music_choice($user_id)
	{	
		$this->builder11->where("userid",$user_id);
		$this->builder11->orderBy('music_choice_id', 'DESC');
		return $this->builder11->get()->getResultArray();
	}
	
	public function add_user_music_choice($data)
	{
		$this->builder11->insert($data);
		return $this->db->insertID();
	}
	
	public function delete_user_music_choice($user_id)
	{
		$this->builder11->where("userid",$user_id);
		$this->builder11->delete();
	}
	
	public function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}
	
	public function crud_create_music_category($data)
	{
		$this->builder5->insert($data);
		return $this->db->insertID();
	}
	
	public function crud_update_music_category($data,$catid)
	{
		$this->builder5->where("music_category_id",$catid);
		$this->builder5->update($data);
		return $this->db->insertID();
	}
	
	public function crud_delete_music_category($catid)
	{
		$this->builder5->where("music_category_id",$catid);
		$this->builder5->delete();
	}
	
	public function crud_create_post_report($data)
	{
		$this->builder2->insert($data);
		return $this->db->insertID();
	}
	
	function crud_read_post_report($user_id,$song_id)
	{	
		$row = $this->builder2->where("userid",$user_id)->where("videoid",$video_id)->get()->getRow();
		if (isset($row->user_id)) {
			return $row->user_id;
		}
		else{
			return 0;
		}
	}
	
	function crud_read_song_report($user_id,$song_id)
	{	
		$row = $this->builder->where("userid",$user_id)->where("videoid",$video_id)->get()->getRow();
		if (isset($row->user_id)) {	
			return $row->user_id;
		}
		else{
			return 0;
		}
	}
	
	function crud_read_follow($user_id,$friend_id)
	{	
		$row = $this->builder7->where("userid",$user_id)->where("friend_id",$friend_id)->where("follow_status",1)->get()->getRow();
		if (isset($row->following_id)) {
			return $row->following_id;
		}
		else{
			return 0;
		}
	}
	
	public function crud_read_follow_status($user_id,$friend_id)
	{	
		$this->builder7->where("userid",$user_id);
		$this->builder7->where("friend_id",$friend_id);
		return $this->builder7->get()->getResultArray();
	}
	
	
	public function crud_create_follow($data)
	{
		$this->builder7->insert($data);
		return $this->db->insertID();
	}
	
	public function crud_update_follow($data,$following_id)
	{	
		$this->builder7->where("following_id",$following_id);
		$this->builder7->update($data);
		return $this->db->insertID();
	}
	
	public function crud_create_post_comment_reply($data)
	{
		$this->builder8->insert($data);
		return $this->db->insertID();
	}
	
	public function crud_create_post_comment($data)
	{
		$this->builder3->insert($data);
		return $this->db->insertID();
	}
	
	public function curd_read_user_comment($user_id,$video_id)
	{	
		$this->builder3->where("user_id",$user_id);
		$this->builder3->where("video_id",$video_id);
		$this->builder3->orderBy('comments_id', 'DESC');
		return $this->builder3->get()->getResultArray();
	}
	
	public function curd_read_comment_reply($comments_id)
	{	
		$this->builder8->where("comment_id",$comments_id);
		$this->builder8->orderBy('reply_id', 'DESC');
		return $this->builder8->get()->getResultArray();
	}
	
	public function curd_read_all_comment($video_id)
	{	
		$this->builder3->where("video_id",$video_id);
		$this->builder3->orderBy('comments_id', 'DESC');
		return $this->builder3->get()->getResultArray();
	}
	
	public function curd_read_comment($comment_id)
	{	
		$this->builder3->where("comments_id",$comment_id);
		$this->builder3->orderBy('comments_id', 'DESC');
		return $this->builder3->get()->getResultArray();
	}
	
	public function curd_read_reply($reply_id)
	{	
		$this->builder8->where("reply_id",$reply_id);
		$this->builder8->orderBy('reply_id', 'DESC');
		return $this->builder8->get()->getResultArray();
	}
	
	public function crud_create_post_likes_unlikes($data)
	{
		$this->builder4->insert($data);
		return $this->db->insertID();
	}
	
	public function crud_create_comment_likes_unlikes($data)
	{
		$this->builder9->insert($data);
		return $this->db->insertID();
	}
	
	public function crud_create_reply_likes_unlikes($data)
	{
		$this->builder10->insert($data);
		return $this->db->insertID();
	}
	
	public function get_user_likes($video_id)
	{	
		return $this->db->query("SELECT post_likes.like_status,users.user_id,users.name,users.profile_img FROM post_likes LEFT JOIN users ON users.user_id = post_likes.user_id WHERE post_likes.video_id =".$video_id)->getResultArray();
		
		/* $this->builder4->where("video_id",$video_id);
		$this->builder4->orderBy('comments_id', 'DESC');
		return $this->builder4->get()->getResultArray(); */
	}
	
	function crud_read_post_likes($video_id)
	{	
		$this->builder4->where("video_id",$video_id);
		$this->builder4->orderBy('likes_id', 'DESC');
		return $this->builder4->get()->getResultArray();
	}
	
	function crud_read_post_likes_unlikes($user_id,$video_id)
	{	
		$row = $this->builder4->where("user_id",$user_id)->where("video_id",$video_id)->get()->getRow();
		if (isset($row->likes_id)) {
			return $row->likes_id;
		}
		else{
			return 0;
		}
	}
	
	function crud_read_comment_likes_unlikes($user_id,$comment_id)
	{	
		$row = $this->builder9->where("user_id",$user_id)->where("comment_id",$comment_id)->where("like_status",1)->get()->getRow();
		if (isset($row->comment_likes_id)) {
			return $row->comment_likes_id;
		}
		else{
			return 0;
		}
	}
	
	function crud_read_reply_likes_unlikes($user_id,$reply_id)
	{	
		$row = $this->builder10->where("user_id",$user_id)->where("reply_id",$reply_id)->where("like_status",1)->get()->getRow();
		if (isset($row->comment_reply_likes_id)) {
			return $row->comment_reply_likes_id;
		}
		else{
			return 0;
		}
	}
	
	public function crud_update_comment_likes_unlikes($data,$comment_likes_id)
	{	
		$this->builder9->where("comment_likes_id",$comment_likes_id);
		$this->builder9->update($data);
		return $this->db->insertID();
	}
	
	public function crud_update_reply_likes_unlikes($data,$likes_id)
	{	
		$this->builder10->where("comment_reply_likes_id",$likes_id);
		$this->builder10->update($data);
		return $this->db->insertID();
	}
	
	public function crud_update_post_likes_unlikes($data,$likes_id)
	{	
		$this->builder4->where("likes_id",$likes_id);
		$this->builder4->update($data);
		return $this->db->insertID();
	}
	
	public function crud_read_likes($user_id,$video_id)
	{
		$row = $this->builder4->where("user_id",$user_id)->where("video_id",$video_id)->where("like_status",1)->get()->getRow();
		if (isset($row->likes_id)) {
			return $row->likes_id;
		}
		else{
			return 0;
		}
	}
	
	
	public function crud_create_favorite($data)
	{
		$this->builder6->insert($data);
		return $this->db->insertID();
	}
	
	public function crud_read_favorite($user_id,$video_id)
	{
		$row = $this->builder6->where("userid",$user_id)->where("video_id",$video_id)->where("favorite_status",1)->get()->getRow();
		if (isset($row->favorite_id)) {
			return $row->favorite_id;
		}
		else{
			return 0;
		}
	}
	
	public function crud_update_favorite($data,$favorite_id)
	{	
		$this->builder6->where("favorite_id",$favorite_id);
		$this->builder6->update($data);
		return $this->db->insertID();
	}
	
	public function curd_read_user_favorite($user_id)
	{	
		$this->builder6->where("userid",$user_id);
		$this->builder6->where("favorite_status",1);
		$this->builder6->orderBy('favorite_id', 'DESC');
		return $this->builder6->get()->getResultArray();
	}
	
	/* function crud_read_post_comment($user_id,$video_id)
	{	
		$row = $this->builder3->where("userid",$user_id)->where("videoid",$video_id)->get()->getRow();
		if (isset($row->user_id)) {
			return $row->user_id;
		}
		else{
			return 0;
		}
	} */
	
	public function upload_video($data)
	{
		$this->builder1->insert($data);
		return $this->db->insertID();
	}
	
	public function crud_read_user_video($userid,$video_id ='')
	{	
		if($video_id !='')
		{
			$this->builder1->where("video_id",$video_id);
			$this->builder1->where("user_id",$userid);
			$this->builder1->where("video_status",0);
			$this->builder1->orderBy('video_id', 'DESC');
			return $this->builder1->get()->getResultArray();
		}else{
			$this->builder1->where("user_id",$userid);
			$this->builder1->where("video_status",0);
			$this->builder1->orderBy('video_id', 'DESC');
			return $this->builder1->get()->getResultArray();
		}
		
	}
	
	public function crud_read_video($video_id='')
	{	
		if($video_id !='')
		{
			$this->builder1->where("video_id",$video_id);
			$this->builder1->where("video_status",0);
			$this->builder1->orderBy('video_id', 'DESC');
			return $this->builder1->get()->getResultArray();
		}else{
			$this->builder1->orderBy('video_id', 'DESC');
			return $this->builder1->get()->getResultArray();
		}
	}
	
	public function crud_delete_user_video($video_id)
	{	
		$this->builder1->where('video_id', $video_id);
		$this->builder1->where("video_status",0);
		$this->builder1->delete();
		
	}
	public function curd_read_music_category($music_category_id ='')
	{	
		if($music_category_id !='')
		{
			$this->builder5->where("music_category_id",$music_category_id);
			$this->builder5->orderBy('category_name', 'ASC');
			return $this->builder5->get()->getResultArray();
		}else{
			
			$this->builder5->orderBy('category_name', 'ASC');
			return $this->builder5->get()->getResultArray();
		}
	}
	
	public function crud_update_video($data,$video_id)
	{	
		$this->builder1->where("video_id",$video_id);
		$this->builder1->update($data);
	}
	
	public function crud_update_comment($data,$comment_id)
	{	
		$this->builder3->where("comments_id",$comment_id);
		$this->builder3->update($data);
	}
	
	public function crud_update_comment_reply($data,$reply_id)
	{	
		$this->builder8->where("reply_id",$reply_id);
		$this->builder8->update($data);
	}
	
	public function crud_read($song_id = '')
	{	
		if($song_id !='')
		{
			$this->builder->where("song_id",$song_id);
			//$this->builder->orderBy('song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}else{
			$this->builder->orderBy('song_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
		
	}

	public function crud_update($data, $song_id)
	{	
		$this->builder->where("song_id",$song_id);
		$this->builder->update($data);
	}

	public function crud_delete($song_id)
	{	
		$this->builder1->where('video_id', $song_id);
		$this->builder1->delete();
	}
	
	
	public function get_user_video($userid)
	{	
		$this->builder1->where("user_id",$userid);
		$this->builder1->where("video_status",0);
		$this->builder1->orderBy('total_likes', 'ASC');
		$this->builder1->LIMIT(10,0);
		return $this->builder1->get()->getResultArray();
	}
	
	
}


?>