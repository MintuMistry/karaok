<?php  

namespace App\Models;

use CodeIgniter\Model;

class User_report_post extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('post_report');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read()
	{	
		$this->builder->orderBy('post_report_id', 'DESC');
		return $this->builder->get()->getResultArray();
	}

	public function crud_update($data, $post_report_id)
	{	
		$this->builder->where("post_report_id",$post_report_id);
		$this->builder->update($data);
	}

	public function crud_delete($post_report_id)
	{	
		$this->builder->where('post_report_id', $post_report_id);
		$this->builder->delete();
	}
}


?>