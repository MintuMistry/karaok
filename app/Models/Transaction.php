<?php  

namespace App\Models;

use CodeIgniter\Model;

class Transaction extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('transaction');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read()
	{	
		$this->builder->orderBy('transaction_id', 'DESC');
		return $this->builder->get()->getResultArray();
	}

	public function crud_update($data, $transaction_id)
	{	
		$this->builder->where("transaction_id",$transaction_id);
		$this->builder->update($data);
	}

	public function crud_delete($transaction_id)
	{	
		$this->builder->where('transaction_id', $transaction_id);
		$this->builder->delete();
	}
}


?>