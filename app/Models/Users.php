<?php  

namespace App\Models;


use CodeIgniter\Model;

class Users extends Model
{
	protected $db;

    public function __construct()
    {
    
       	$this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('users');
   		$this->builder1 =  $this->db->table('social_login');
   		$this->builder2 =  $this->db->table('reported_users');
   		$this->builder3 =  $this->db->table('user_following');
   		
    }
	
	public function crud_read_top_artist($min_range ='',$max_range ='')
	{
		if($min_range !='' && $max_range !='')
		{
			$this->builder->orderBy('total_follower', 'DESC');
			$this->builder->LIMIT($max_range,$min_range);
			return $this->builder->get()->getResultArray();
		}else{
			$this->builder->orderBy('total_follower', 'DESC');
			$this->builder->LIMIT(5,0);
			return $this->builder->get()->getResultArray();
		}
	}
	
	public function crud_read_top_singer($min_range ='',$max_range ='')
	{
		if($min_range !='' && $max_range !='')
		{
			$this->builder->orderBy('total_follower', 'DESC');
			$this->builder->LIMIT($max_range,$min_range);
			return $this->builder->get()->getResultArray();
		}else{
			$this->builder->orderBy('total_follower', 'DESC');
			$this->builder->LIMIT(5,0);
			return $this->builder->get()->getResultArray();
		}
	}
	
	
	function crud_read_follower($userid)
	{	
		$this->builder3->where("friend_id",$userid);
		$this->builder3->where("follow_status",1);
		$this->builder3->orderBy('friend_id','DESC');
		return $this->builder3->get()->getResultArray();
	}
	
	function crud_read_following($userid)
	{	
		$this->builder3->where("userid",$userid);
		$this->builder3->where("follow_status",1);
		$this->builder3->orderBy('userid','DESC');
		
		return $this->builder3->get()->getResultArray();
	}
	
	function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	function crud_read($userid = '')
	{	
		if($userid > 0){
			$this->builder->where("user_id",$userid);
			$this->builder->orderBy('user_id','DESC');
			return $this->builder->get()->getResultArray();
			
		}
		else
			$this->builder->orderBy('user_id','DESC');
			return $this->builder->get()->getResultArray();
	}
	
	
	function crud_update($data, $user_id)
	{	
		$this->builder->where("user_id",$user_id);
		$this->builder->update($data);
	}
	
	
	function crud_delete($user_id)
	{	
		$this->builder->where('user_id', $user_id);
		$this->builder->delete();
	}

	function read_by_mail($mail = '')
	{
		return $this->builder->where("user_email", $mail)->get()->getResultArray();
	}

	function read_by_mail_or_phone($mail = '', $mobile = '')
	{
		if($mail)
			return $this->builder->where("user_email", $mail)->get()->getResultArray();
		else 
			return $this->builder->where("mobile", $mobile)->get()->getResultArray();
	}

	function read_by_mail_or_username($mail = '', $user_name = '', $password = '')
	{
		if($mail)
			return $this->builder->where("user_email", $mail)->where('password', $password)
					->get()->getResultArray();
		else 
			return $this->builder->where("user_name", $user_name)->where('password', $password)
					->get()->getResultArray();
	}
	
	
	function check_username_exists($username)
	{	
		$row = $this->builder->where("user_name",$username)->get()->getRow();
		if (isset($row->user_id)) {
			return $row->user_id;
		}
		else{
			return 0;
		}
	}
	
	function check_email_exists($email)
	{	
		$row = $this->builder->where("user_email",$email)->get()->getRow();
		if (isset($row->user_id)) {
			return $row->user_id;
		}
		else{
			return 0;
		}
	}
	
	function create_social_login($data)
	{
		$this->builder1->insert($data);
		return $this->db->insertID();
	}
	
	public function read_home_data($data)
	{
		if(empty($data['min_range'])){
            $min_range = '0';
        }
        if(empty($data['max_range'])){
            $max_range = '50';
        }
		$unit = 'miles';
        
        if($unit == 'meter'){
        	$earth_radius = '6371000';
        }
        else{
            $earth_radius = '3958.755866';
        }
		$latitude = $data['latitude'];
		$longitude = $data['longitude'];
		$userid = $data['userid'];
		
		return $this->db->query("SELECT  *, ( $earth_radius * acos ( cos ( radians($latitude) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($longitude) ) + sin ( radians($latitude) ) * sin( radians( latitude ) ) ) ) AS distance FROM users WHERE user_id != $userid HAVING distance >= $min_range AND distance < $earth_radius ")->getResultArray();
	}
	
	function report_user($data)
	{	
		$this->builder2->insert($data);
		return $this->db->insertID();
	}
}


?>