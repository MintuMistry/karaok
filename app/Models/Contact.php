<?php  

namespace App\Models;

use CodeIgniter\Model;

class Contact extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('contact_query');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read()
	{	
		$this->builder->orderBy('contact_id', 'DESC');
		return $this->builder->get()->getResultArray();
	}

	public function crud_update($data, $contact_id)
	{	
		$this->builder->where("contact_id",$contact_id);
		$this->builder->update($data);
	}

	public function crud_delete($contact_id)
	{	
		$this->builder->where('contact_id', $contact_id);
		$this->builder->delete();
	}
}


?>