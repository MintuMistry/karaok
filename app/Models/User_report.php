<?php  

namespace App\Models;

use CodeIgniter\Model;

class User_report extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('reported_users');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($user_id = '')
	{	
		if($user_id){
			$this->builder->where('userid', $user_id);
			return $this->builder->get()->getResultArray();
		}
		else{
			$this->builder->orderBy('report_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $report_id)
	{	
		$this->builder->where("report_id",$report_id);
		$this->builder->update($data);
	}

	public function crud_delete($report_idreport_id)
	{	
		$this->builder->where('report_id', $report_id);
		$this->builder->delete();
	}
}


?>