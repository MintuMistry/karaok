<?php  

namespace App\Models;

use CodeIgniter\Model;

class Login extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('admin_detail');
    }

	function admin_verify($email,$password){
		$row = $this->builder->where("admin_email",$email)
									->where("password", $password)
									->get()->getRow();
		if (isset($row->admin_id)) {
			return $row->admin_id;
		}
		else{
			return 0;
		}
	}

	function admin_data($admin_id){
		return $this->builder->where("admin_id",$admin_id)->get()->getRow();
	}

	function crud_update($data, $admin_id){
		$this->builder->where("admin_id",$admin_id);
		$this->builder->update($data);
	}

}


?>