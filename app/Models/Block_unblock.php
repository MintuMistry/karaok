<?php  

namespace App\Models;

use CodeIgniter\Model;

class Block_unblock extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('block_list');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($user_id = '', $followerid = '')
	{	
		if(!empty($user_id) && empty($followerid)){
			$this->builder->where('userid', $user_id);
			$this->builder->where('block_status', 1);
			return $this->builder->get()->getResultArray();
		}
		elseif($user_id && $followerid){
			$this->builder->where('block_status', 1);
			$this->builder->where('userid', $user_id)->where('followerid', $followerid);
			
			return $this->builder->get()->getResultArray();
		}
		else {
			$this->builder->orderBy('block_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
	}

	public function crud_update($data, $block_id)
	{	
		$this->builder->where("block_id",$block_id);
		$this->builder->update($data);
	}

	public function crud_delete($block_id)
	{	
		$this->builder->where('block_id', $block_id);
		$this->builder->delete();
	}
}


?>