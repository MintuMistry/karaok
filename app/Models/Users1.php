<?php  

namespace App\Models;


use CodeIgniter\Model;

class Users1 extends Model
{
	protected $db;

    public function __construct()
    {
    
       	$this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('users');
   		
    }

	function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}

	function crud_read($userid = '')
	{	
		if($userid > 0){
			$this->builder->where("user_id",$userid);
			$this->builder->orderBy('user_id','DESC');
			return $this->builder->get()->getResultArray();
			
		}
		else
			$this->builder->orderBy('user_id','DESC');
			return $this->builder->get()->getResultArray();
	}
	
	
	function crud_update($data, $user_id)
	{	
		$this->builder->where("user_id",$user_id);
		$this->builder->update($data);
	}
	
	
	function crud_delete($user_id)
	{	
		$this->builder->where('user_id', $user_id);
		$this->builder->delete();
	}

	function read_by_mail_or_phone($mail,$phone,$country_code)
	{
		if(!empty($mail))
			return $this->builder->where("user_email", $mail)
					->get()->getResultArray();
		else
			return $this->builder->where("mobile", $phone)->where("country_code", $country_code)->get()->getResultArray();
	}

}


?>