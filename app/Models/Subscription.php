<?php  

namespace App\Models;

use CodeIgniter\Model;

class Subscription extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('subscription_plan');
		$this->builder2 =  $this->db->table('user_plan');
   		$this->builder3 =  $this->db->table('transaction');
   		$this->builder4 =  $this->db->table('user_plan_history');
   		$this->builder5 =  $this->db->table('user_messages');
   		$this->builder6 =  $this->db->table('user_swipes');
    }
	
	function crud_create($data)
	{
		$this->builder->insert($data);
		return $this->db->insertID();
	}
	
	function crud_read($plan_id = '')
	{	
		if($plan_id!=''){
			$this->builder->where("plan_id",$plan_id);
			return $this->builder->get()->getResultArray();
		}
		else
			//$this->builder->orderBy("plan_id","DESC");
			return $this->builder->get()->getResultArray();		
	}

	function crud_update($data, $plan_id)
	{	
		$this->builder->where("plan_id",$plan_id);
		$this->builder->update($data);
	}

	public function crud_delete($plan_id)
	{	
		$this->builder->where('plan_id', $plan_id);
		$this->builder->delete();
	}
	
	function crud_read_user_swipe_history($user_id = '')
	{	
		if($user_id > 0){
			
			$response = $this->db->query("SELECT * FROM user_swipes WHERE userid =" .$user_id)->getResultArray();
			return $response;	
			/* $this->builder6->where(DATE("userid"),$user_id);
			$this->builder6->where("userid",$user_id);
			return $this->builder6->get()->getResultArray(); */
		}
		else
			return $this->builder6->get()->getResultArray();		
	}
	
	public function create_user_msg($data)
	{
		$this->builder5->insert($data);
		return true;
	}
	
	public function create_user_plan_history($data)
	{
		$this->builder4->insert($data);
		return true;
	}
	
	function crud_read_user_plan($user_id)
	{
		$this->builder3->where("userid",$user_id);
		return $this->builder3->get()->getResultArray();
	}
	
	
	
	function update_user_plan_history($data, $history_id)
	{	
		$this->builder4->where("user_plan_history_id",$history_id);
		$this->builder4->update($data);
	}

	public function create_user_plan($data)
	{
		$this->builder2->insert($data);
		return true;
	}
	
	public function create_user_tx($data)
	{
		$this->builder3->insert($data);
		return true;
	}
	
	function update_user_tx($data, $userid)
	{	
		$this->builder3->where("userid",$userid);
		$this->builder3->update($data);
	}

	function read_user_plan($user_id = '', $plan_id = '')
	{	
		if($user_id > 0 && empty($plan_id)){
			$this->builder2->where("userid",$user_id);
			return $this->builder2->get()->getResultArray();
		}
		elseif($user_id != 0 && $plan_id != 0) {
			$this->builder2->where("userid",$user_id)->where("planid",$plan_id);
			return $this->builder2->get()->getResultArray();
		}
		else {
			return $this->builder2->get()->getResultArray();
		}	
	}

	function update_user_plan($data, $user_plan_id)
	{	
		$this->builder2->where("user_plan_id",$user_plan_id);
		$this->builder2->update($data);
	}
	function crud_read_user_plan_history($user_id)
	{
		$this->builder4->where("userid",$user_id);
		return $this->builder4->get()->getResultArray();
	}
}


?>