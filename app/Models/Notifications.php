<?php  

namespace App\Models;

use CodeIgniter\Model;

class Notifications extends Model
{
	protected $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
   		$this->builder =  $this->db->table('notifications');
    }

	public function crud_create($data)
	{
		$this->builder->insert($data);
	}

	public function crud_read($notification_id ='')
	{	
		if($notification_id !='')
		{
			$this->builder->where('notification_id', $notification_id);
			return $this->builder->get()->getResultArray();
			
		}else{
			$this->builder->orderBy('notification_id', 'DESC');
			return $this->builder->get()->getResultArray();
		}
		
	}
	
	public function crud_read_notification($user_id)
	{	
		$this->builder->where('userid', $user_id);
		$this->builder->groupBy('friendid');
		$this->builder->orderBy('notification_id', 'DESC');
		return $this->builder->get()->getResultArray();
		
	}
	
	public function crud_filter_notification($user_id,$last_id)
	{	
		if($last_id == 0)
		{
			$this->builder->where('userid', $user_id);
			$this->builder->orderBy('notification_id', 'DESC');
			$this->builder->LIMIT(5,0);
			return $this->builder->get()->getResultArray();
			
		}else{
			$this->builder->where('userid', $user_id);
			$this->builder->where('notification_id <', $last_id);
			$this->builder->orderBy('notification_id', 'DESC');
			$this->builder->LIMIT(5,0);
			return $this->builder->get()->getResultArray();
		}
		
	}
	
	public function crud_update($data, $notification_id)
	{	
		$this->builder->where("notification_id",$notification_id);
		$this->builder->update($data);
	}

	public function crud_delete($notification_id)
	{	
		$this->builder->where('notification_id', $notification_id);
		$this->builder->delete();
	}

	public function read_user_notifications($user_id)
	{	
		$this->builder->where('userid', $user_id);
		$this->builder->orderBy('notification_id', 'DESC');
		return $this->builder->get()->getResultArray();
	}
}


?>