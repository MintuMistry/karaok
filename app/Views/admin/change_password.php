<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="#">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/change_password_management">Change Password</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<style>
#toast-container > .toast-success {
    margin-left: auto !important;
    margin-top: 0px !important;
    top: 50px!important;
    right:20px;
}
.toast-close-button{
	position: absolute !important;
	right: 6px !important;
	top:6px !important;
}
.toast-message{
	color: #fff !important;
	padding :15px 6px !important;
}
</style>

		<?php $this->session = \Config\Services::session()?>
		<?php if($this->session->success_msg){ ?>
			<div class="alert alert-success alert-dismissible fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<strong>Success! </strong><?php echo $this->session->success_msg; ?></div>
		<?php } if($this->session->error_msg){?>
		<div class="alert alert-success alert-dismissible fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<strong>Fail! </strong><?php echo $this->session->error_msg; ?></div>
		<?php } ?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Change Password
				</h1>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/changePassword">
						<div class="space-4"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label " style = "text-align: left;" for="form-field-2"> Old Password  *</label>
							<div class="col-sm-9">
								<input type="password" id="form-field-2" placeholder="Old Password" class="col-xs-10 col-sm-5" name="oldpassword" required="" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label " style = "text-align: left;" for="form-field-2"> New Password  *</label>
							<div class="col-sm-9">
								<input type="password" id="password" placeholder="Password" class="col-xs-10 col-sm-5" name="password" required="" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label " style = "text-align: left;" for="form-field-2"> Confirm Password  *</label>
							<div class="col-sm-9">
								<input type="password" id="cpassword" placeholder="Confirm Password" class="col-xs-10 col-sm-5" name="cpassword" required="" />
								<span id='message'></span>
							</div>
						</div>
						<div class="space-4"></div>
						<div class="clearfix form-actions">
							<div class="col-md-offset-3 col-md-9">
								<button class="btn btn-info" type="submit" >
									<i class="ace-icon fa fa-check bigger-110"></i>
									Submit
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#cpassword').on('keyup', function () {
  	if ($('#password').val() == $('#cpassword').val()) {
    	$('#message').html('Password matched').css('color', 'green');
  	} else {
    	$('#message').html('Password not matched').css('color', 'red');
	}
});
</script>