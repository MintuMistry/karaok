<div class="footer">
	<div class="footer-inner">
		<div class="footer-content">
			<span class="bigger-120">
				<span class="blue bolder">Karaoke</span>
				Application &copy; 2020-2021
			</span>

			&nbsp; &nbsp;
			
		</div>
	</div>
</div>

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
		<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
	</a>
</div><!-- /.main-container -->
<!-- basic scripts -->
<!--[if !IE]> -->

<!-- <script src="<?php //echo base_url(); ?>//public/assets/js/jquery-2.1.4.min.js"></script>-->
<!-- <![endif]-->
<!--[if IE]>
<script src="//public/assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->

<script type="text/javascript">
if ('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url(); ?>/public/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url(); ?>/public/assets/js/bootstrap.min.js"></script>
<!-- page specific plugin scripts -->
<!--[if lte IE 8]>
		  <script src="//public/assets/js/excanvas.min.js"></script>
		<![endif]-->
<script src="<?php echo base_url(); ?>/public/assets/js/jquery-ui.custom.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.sparkline.index.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.flot.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.flot.pie.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/jquery.flot.resize.min.js"></script>


<!--  -->
<!-- ace scripts -->
<script src="<?php echo base_url(); ?>/public/assets/js/ace-elements.min.js"></script>
<script src="<?php echo base_url(); ?>/public/assets/js/ace.min.js"></script>

<!-- toastr script -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	
</script>


<!-- inline scripts related to this page -->
<script type="text/javascript">
</script>
<script type="text/javascript">
	$(function() {
		
		//toastr.options.preventDuplicates = true;
		//toastr.options.closeButton = true;
		
		setTimeout(function() {
		    $('.alert-success').fadeOut('slow');
		}, 2000);

		$('.fa-trash-o').click(function(event){
			//alert("dsbcjd");
			if (confirm("Are you sure you want to delete?")) {
				return true;
			}
			else {
				event.preventDefault();
			}
		});

		/*--Data Table --*/
		var myTable = 
		$('#dynamic-table')
			.DataTable({
				"oSearch": { "bSmart": false, "bRegex": true }
		});
	});

</script>


</body>

</html>