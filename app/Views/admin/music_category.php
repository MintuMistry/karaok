
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/music_category">Music Category</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<style>
		#toast-container > .toast-success {
		    margin-left: auto !important;
		    margin-top: 0px !important;
		    top: 50px!important;
		    right:20px;
		}
		.toast-close-button{
			position: absolute !important;
			right: 6px !important;
			top:6px !important;
		}
		.toast-message{
			color: #fff !important;
			padding :15px 6px !important;
		}
		</style>

		<?php $this->session = \Config\Services::session()?>
		<?php if($this->session->success_msg){ ?>
			<div class="alert alert-success alert-dismissible fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<strong>Success! </strong><?php echo $this->session->success_msg; ?></div>
		<?php } if($this->session->error_msg){?>
		<div class="alert alert-success alert-dismissible fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<strong>Fail! </strong><?php echo $this->session->error_msg; ?></div>
		<?php } ?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Music Category List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#addcategory">Add Category </div>
			</div>
			<!------------- Modal for Add Music Category ------------------>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="addcategory" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Add Music Category <button style="float:right;" type="button" class="close" data-dismiss="modal" aria-label="Close">
		                      <span aria-hidden="true">×</span>
		                    </button></h5>
							
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/addCategory" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Category Name *</label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Category name" class="col-xs-10 col-sm-5" name="category_name" required="" />
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Category Image *</label>

											<div class="col-sm-9">
												<input type="file" name="cat_image" class="file-input" accept=".jpeg,.png" required="">
											</div>
										</div>
										
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
<!------------- Modal for Edit Music Category ----------------------->
			<?php
				foreach($music_category as $cat){ 
			?>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="edit_category<?=$cat['music_category_id']?>" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Edit Music Category <button style="float:right;" type="button" class="close" data-dismiss="modal" aria-label="Close">
		                      <span aria-hidden="true">×</span>
		                    </button></h5>
							
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/editCategory/<?=$cat['music_category_id']?>" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Category Name *</label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Plan name" class="col-xs-10 col-sm-5" name="category_name" value="<?= $cat['category_name']?>" required=""/>
											</div>
										</div>
									<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Category Image *</label>

											<div class="col-sm-9">
												<input type="file" name="cat_image" class="file-input" accept=".jpeg,.png" required="">
											</div>
										</div>
										
										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
<!---------------------Music Category List ------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Category Name</th>
								<th scope="col">Category Image</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($music_category as $cat){ 
								$snum += 1;
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><img src="<?php echo base_url()."/writable/uploads/".$cat['category_img']?>" height="100px" width="100px" alt="Cat Imamge"></td>
								<td><?= $cat['category_name']?></td>
								<td>
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120" data-toggle="modal" title="Edit"  style="cursor:pointer;" data-target="#edit_category<?=$cat['music_category_id']?>"></i>

									</span>
									<a href="<?php base_url(); ?>deleteCategory/<?=$cat['music_category_id']?>" title="Delete" class="ace-icon fa fa-delete-o bigger-120" style="cursor:pointer;">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
