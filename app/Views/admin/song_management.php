<?php
use App\Models\Songs;
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/song_management">Song Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<style>
		#toast-container > .toast-success {
		    margin-left: auto !important;
		    margin-top: 0px !important;
		    top: 50px!important;
		    right:20px;
		}
		.toast-close-button{
			position: absolute !important;
			right: 6px !important;
			top:6px !important;
		}
		.toast-message{
			color: #fff !important;
			padding :15px 6px !important;
		}
		</style>

		<?php $this->session = \Config\Services::session()?>
		<?php if($this->session->success_msg){ ?>
			<div class="alert alert-success alert-dismissible fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<strong>Success! </strong><?php echo $this->session->success_msg; ?></div>
		<?php } if($this->session->error_msg){?>
		<div class="alert alert-success alert-dismissible fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<strong>Fail! </strong><?php echo $this->session->error_msg; ?></div>
		<?php } ?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Songs List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#addsong">Add Song </div>
			</div>
			<!------------- Modal for Add Song ------------------>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="addsong" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Add Song 
								<button style="float:right;" type="button" class="close" data-dismiss="modal" aria-label="Close">
			                     	<span aria-hidden="true">×</span>
			                    </button>
		                    </h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/addSong" enctype="multipart/form-data">
									
									<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Song Category *</label>

											<div class="col-sm-9">
												<select type="text"  placeholder="Select Song Category" class="col-xs-10 col-sm-5" name="song_category" required="" >
												<?php foreach($music_category as $cat){ ?>
												<option value="<?=$cat['music_category_id']?>"><?=$cat['category_name']?></option>
												<?php } ?>
												</select>
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Video *</label>

											<div class="col-sm-9">
												<input type="file" name="song_track" class="file-input" accept="video/*" required="">
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Song Name *</label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Song name" class="col-xs-10 col-sm-5" name="song_name" required="" />
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Song Artist * </label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Song Artist" class="col-xs-10 col-sm-5" name="song_artist" required=""/>
											</div>
										</div>
										
										
										
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Video Thumb *</label>

											<div class="col-sm-9">
												<input type="file" name="song_image" class="file-input" accept=".jpeg,.png" required="">
											</div>
										</div>
										
										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
<!------------- Modal for Edit Song ----------------------->
			<?php
				foreach($song_details as $song){ 
			?>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="edit_song<?=$song['song_id']?>" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Edit Song 
								<button style="float:right;" type="button" class="close" data-dismiss="modal" aria-label="Close">
			                      	<span aria-hidden="true">×</span>
			                    </button>
							</h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/editSong/<?=$song['song_id']?>" enctype="multipart/form-data">
									<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Song Category *</label>

											<div class="col-sm-9">
												<select type="text"  placeholder="Select Song Category" class="col-xs-10 col-sm-5" name="song_category" required="" >
												<?php foreach($music_category as $cat){ 
												if($cat['music_category_id'] == $song['music_cat_id']){
												?>
												<option value="<?=$cat['music_category_id']?>" selected ><?=$cat['category_name']?></option>
												<?php }else{?>
												<option value="<?=$cat['music_category_id']?>"><?=$cat['category_name']?></option>
												<?php }}?>
												</select>
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Song Name *</label>
											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Song name" class="col-xs-10 col-sm-5" name="song_name" value="<?= $song['song_name']?>" required="" />
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Song Track </label>
											<div class="col-sm-9">
												<input type="file" name="song_track" class="file-input" accept=".mp3,audio/*,video/*" >
											</div>
										</div>
											
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Upload Image </label>

											<div class="col-sm-9">
												<input type="file" name="song_image" class="file-input" accept=".jpg,.jpeg,.png">
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Song Artist * </label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Song Artist" class="col-xs-10 col-sm-5" name="song_artist" required="" value="<?= $song['song_artist']?>" />
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Song Genre * </label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Song Genre" class="col-xs-10 col-sm-5" required="" name="song_genre" value="<?= $song['song_genre']?>"/>
											</div>
										</div>

										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												<!-- 
												&nbsp; &nbsp; &nbsp;
												<button class="submit" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button> -->
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
<!------------------------- Songs List ------------------------------>
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Song Category</th>
								<th scope="col">Song Name</th>
								<th scope="col">Video</th>
								<th scope="col">Video Thumb</th>
								<th scope="col">Song Artist</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($song_details as $song){ 
								$snum += 1;
								$Song = new Songs();
								//echo $song['music_cat_id'];
								
								$catname = $Song->curd_read_music_category($song['music_cat_id']);
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $catname[0]['category_name']?></td>
								<td><?= $song['song_name']?></td>
								<td><video class="bg-video" style="height: 110px;width: 125px;" poster="<?php echo base_url()?>/assets/images/video_play.png" autoplay="" loop="" muted="">
										<source src="<?php echo base_url()."/writable/uploads/".$song['video_file']?>">
									</video></td>

								<td><img src="<?php echo base_url()."/writable/uploads/".$song['video_thumb']?>" height="100px" width="100px" alt="Thumbnail"></td>
								<td><?= $song['artist_name']?></td>
								<td>
									<!--<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120" data-toggle="modal" data-target="#edit_song<?=$song['song_id']?>"></i>
									</span>-->
									<a href="<?php base_url(); ?>deleteSong/<?=$song['video_id']?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
