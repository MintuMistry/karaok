<?php 
use App\Models\Users;
use App\Models\Posts;
use App\Models\Songs;
$users = new Users();
$posts = new Posts();
$songs = new Songs();
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/user_report_post_management">Report Post Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>

<style>
#toast-container > .toast-success {
    margin-left: auto !important;
    margin-top: 0px !important;
    top: 50px!important;
    right:20px;
}
.toast-close-button{
	position: absolute !important;
	right: 6px !important;
	top:6px !important;
}
.toast-message{
	color: #fff !important;
	padding :15px 6px !important;
}
</style>

		<?php $this->session = \Config\Services::session()?>
		<?php if($this->session->success_msg){ ?>
			<div class="alert alert-success alert-dismissible fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<strong>Success! </strong><?php echo $this->session->success_msg; ?></div>
		<?php } if($this->session->error_msg){?>
		<div class="alert alert-success alert-dismissible fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<strong>Fail! </strong><?php echo $this->session->error_msg; ?></div>
		<?php } ?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Reported Posts List
				</h1>
			</div>
			<!----------- Posts List ------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Reported Post</th>
								<th scope="col">Reported By</th>
								<th scope="col">Comment</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$snum = 0;
							foreach($report_post_details as $report_post){ 
							/* print_r($report_post);
							die(); */
								$snum += 1;
								$reported_by = $users->crud_read($report_post['userid']);
								$user_videos = $songs->crud_read_video($report_post['videoid']);
								//$reported_to = $users->crud_read($report_post['videoid']);
								//$post_data = $posts->crud_read($report_post['postid']);
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td>
									<?php
										if($user_videos){?>
										<video class="bg-video" style="
    height: 25vh;" poster="<?php echo base_url()?>/assets/images/bg/1.jpeg" autoplay="" loop="" muted="">
											<source src="<?php echo base_url()."/writable/uploads/".$user_videos[0]['video_file']?>">
										</video>
											
										<?php }
										else {
											echo "No Video";
										}
									?>
								</td>
								
								<td>
									<?php
										if($reported_by){
											echo $reported_by[0]['name'];
										}
										else {
											echo "No Name";
										}
									?>
								</td>
								
								<td>
									<?php
										if($report_post['comment']){
											echo $report_post['comment'];
										}
										else {
											echo "No comment";
										}
									?>
								</td>
								
								<td>
									<a href="<?php base_url(); ?>deleteReportPost/<?=$report_post['post_report_id']?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
