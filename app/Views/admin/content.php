
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/content_management">Content Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>

<style>
#toast-container > .toast-success {
    margin-left: auto !important;
    margin-top: 0px !important;
    top: 50px!important;
    right:20px;
}
.toast-close-button{
	position: absolute !important;
	right: 6px !important;
	top:6px !important;
}
.toast-message{
	color: #fff !important;
	padding :15px 6px !important;
}
</style>

		<?php $this->session = \Config\Services::session()?>
		<?php if($this->session->success_msg){ ?>
			<div class="alert alert-success alert-dismissible fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<strong>Success! </strong><?php echo $this->session->success_msg; ?></div>
		<?php } if($this->session->error_msg){?>
		<div class="alert alert-success alert-dismissible fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<strong>Fail! </strong><?php echo $this->session->error_msg; ?></div>
		<?php } ?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Content List
				</h1>
			</div>
			<!--------- Modal for Edit Content -------------------->
			<?php
				foreach($contents as $content){ 
			?>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="edit_content<?=$content['content_id']?>" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Edit Content
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                      <span aria-hidden="true">×</span>
		                    </button></h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/editContent/<?=$content['content_id']?>">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Page Name * </label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Page Name" class="col-xs-10 col-sm-5" name="page_name" readonly="" value="<?= $content['page_name'] ?>" />
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Page Content * </label>

											<div class="col-sm-9">
										<textarea id="form-field-11" class="autosize-transition form-control" style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 52px;" placeholder="Page Content" name="page_content"><?= preg_replace("/\s+/", " ", $content['page_content'])?></textarea>
											</div>
										</div>

										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
			<!----------- Content List ------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Page Name</th>
								<th scope="col">page Content</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$snum = 0;
							foreach($contents as $content){ 
								$snum += 1;
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $content['page_name']?></td>
								<td><?= $content['page_content']?></td>
								<td>
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120" data-toggle="modal" data-target="#edit_content<?=$content['content_id']?>"></i>
									</span>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
