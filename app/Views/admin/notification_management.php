<?php 
use App\Models\Users;
$users = new Users();

?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/notification_management/<?=$user_id;?>">Notification Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<style>
		#toast-container > .toast-success {
		    margin-left: auto !important;
		    margin-top: 0px !important;
		    top: 50px!important;
		    right:20px;
		}
		.toast-close-button{
			position: absolute !important;
			right: 6px !important;
			top:6px !important;
		}
		.toast-message{
			color: #fff !important;
			padding :15px 6px !important;
		}
		</style>

		<?php $this->session = \Config\Services::session()?>
		<?php if($this->session->success_msg){ ?>
			<div class="alert alert-success alert-dismissible fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<strong>Success! </strong><?php echo $this->session->success_msg; ?></div>
		<?php } if($this->session->error_msg){?>
		<div class="alert alert-success alert-dismissible fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<strong>Fail! </strong><?php echo $this->session->error_msg; ?></div>
		<?php } ?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Notifications List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#send_notification">Send Notification </div>
			</div>
			<!------------- Modal for Add Notification ------------------>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="send_notification" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Send Notification 
								<button style="float:right;" type="button" class="close" data-dismiss="modal" aria-label="Close">
			                     	<span aria-hidden="true">×</span>
			                    </button>
		                    </h5>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>/admin/addNotification">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Notification Text </label>
											<div class="col-sm-9">
												<input type="hidden" name="user_id" value="<?= $user_id?>">
												<textarea id="form-field-1" placeholder="Notification Text" class="col-xs-10 col-sm-5" name="message" required=""></textarea>
											</div>
										</div>
										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Send
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!------------------ Notifications List --------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">User Name</th>
								<th scope="col">Notification Text</th>
								<th scope="col">Date</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($notifications_details as $notification){ 
								$snum += 1;
								$user_data = $users->crud_read($notification['userid']);
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td>
									<?php
										if($user_data){
											echo $user_data[0]['name'];
										}
										else {
											echo "";
										}
									?>
								</td>
								<td><?= $notification['message']?></td>
								<td><?= date('m-d-Y', strtotime($notification['created_at']))?></td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
