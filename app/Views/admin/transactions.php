<?php 
use App\Models\Users;
$users = new Users();
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/transaction_management">Transaction Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<style>
			#toast-container > .toast-success {
			    margin-left: auto !important;
			    margin-top: 0px !important;
			    top: 50px!important;
			    right:20px;
			}
			.toast-close-button{
				position: absolute !important;
				right: 6px !important;
				top:6px !important;
			}
			.toast-message{
				color: #fff !important;
				padding :15px 6px !important;
			}
		</style>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Transactions List
				</h1>
			</div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			</button>
			
			<!----------- Transactions List ------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Transaction Token</th>
								<th scope="col">Amount</th>
								<th scope="col">User Name</th>
								<th scope="col">Date</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$snum = 0;
							foreach($transaction_details as $transaction){ 
								$snum += 1;
								$user_data = $users->crud_read($transaction['userid']);
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $transaction['transaction_token']?></td>
								<td><?= $transaction['amount']?></td>
								<td>
									<?php
										if($user_data){
											echo $user_data[0]['name'];
										}
										else {
											echo "";
										}
									?>
								</td>
								<td><?= date('m-d-Y', strtotime($transaction['createdat']))?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
