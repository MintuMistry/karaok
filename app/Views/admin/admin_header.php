<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Dashboard - Karaoke</title>
	<meta name="description" content="overview &amp; stats" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/assets/css/bootstrap.min.css" />
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/assets/font-awesome/4.5.0/css/font-awesome.min.css" />
	<!-- page specific plugin styles -->
	<!-- text fonts -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/assets/css/fonts.googleapis.com.css" />
	
	<!-- ace styles -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
	<!--[if lte IE 9]>
			<link rel="stylesheet" href="//public/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/assets/css/ace-skins.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>/public/assets/css/ace-rtl.min.css" />
	<!--[if lte IE 9]>
		  <link rel="stylesheet" href="//public/assets/css/ace-ie.min.css" />
		<![endif]-->
	<!-- inline styles related to this page -->
	<!-- ace settings handler -->
	<script src="<?php echo base_url(); ?>/public/assets/js/ace-extra.min.js"></script>
	<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
	<!--[if lte IE 8]>
		<script src="//public/assets/js/html5shiv.min.js"></script>
		<script src="//public/assets/js/respond.min.js"></script>
		<![endif]-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	
	<!-- data table -->

	<script src="<?php echo base_url(); ?>/public/assets/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>/public/assets/js/jquery.dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>/public/assets/js/dataTables.buttons.min.js"></script>
	<script src="<?php echo base_url(); ?>/public/assets/js/buttons.flash.min.js"></script>
	<script src="<?php echo base_url(); ?>/public/assets/js/buttons.html5.min.js"></script>
	<script src="<?php echo base_url(); ?>/public/assets/js/buttons.print.min.js"></script>
	<script src="<?php echo base_url(); ?>/public/assets/js/buttons.colVis.min.js"></script>
	<script src="<?php echo base_url(); ?>/public/assets/js/dataTables.select.min.js"></script>
	
	<style type="text/css">
		#toast-container > .toast-success {
		    background-image: none;
		    background-color: #008000;
		    color: black;
		    width: 300px;
		    height: 50px;
		    margin-left: 1000px;
		    margin-top: -300px;
		    position: fixed;
		    text-align: center;
		}
		#toast-container > .toast-error {
		    background-image: none;
		    background-color: #008000;
		    color: black;
		    margin-right: 0px;
		    margin-top: -400px;
		    position: fixed;
		}
		.nav-list>li>a {
		    height: auto !important;
		}
		ul.nav.nav-list>li>a>i {
		    position: absolute;
		    top: 50%;
		    transform: translateY(-50%);
		}
		ul.nav.nav-list>li>a>span {
		    padding-left: 32px;
		    text-align: left !important;
		    width: 100% !important;
		    display: inline-block;
		}
	</style>	
</head>
	<!-- Header File -->

<body class="skin-2">
<div id="navbar" class="navbar navbar-default          ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="<?php echo base_url(''); ?>" class="navbar-brand">
						<small>
							<i class="fa fa-building"></i>
							Karaoke Admin Panel
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
					

						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo base_url(); ?>//public/assets/images/avatars/user.jpg" alt="Jason's Photo" />
								<span class="user-info">
									<small>Welcome,</small>
									Admin
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								

								<li>
									<a href="<?php echo base_url(); ?>/admin/change_password_management">
										<i class="ace-icon fa fa-user"></i>
										Change Password
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="<?php echo base_url("admin/logout"); ?>">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>
		
		<style>
		.form-group {
    margin-bottom: 15px;
    overflow: hidden;
}
		</style>


		<div class="main-container ace-save-state" id="main-container">
	<script type="text/javascript">
	try { ace.settings.loadState('main-container') } catch (e) {}
	</script>
	<div id="sidebar" class="sidebar responsive  ace-save-state">
		<script type="text/javascript">
		try { ace.settings.loadState('sidebar') } catch (e) {}
		</script>
		<!-- Admin sidebar -->
		<?php echo view('admin/admin_sidebar'); ?>
		<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
			<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
		</div>
	</div>