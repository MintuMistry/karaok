
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/subscription_management">Subscription Plan Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>
		<style>
		#toast-container > .toast-success {
		    margin-left: auto !important;
		    margin-top: 0px !important;
		    top: 50px!important;
		    right:20px;
		}
		.toast-close-button{
			position: absolute !important;
			right: 6px !important;
			top:6px !important;
		}
		.toast-message{
			color: #fff !important;
			padding :15px 6px !important;
		}
		</style>

		<?php $this->session = \Config\Services::session()?>
		<?php if($this->session->success_msg){ ?>
			<div class="alert alert-success alert-dismissible fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<strong>Success! </strong><?php echo $this->session->success_msg; ?></div>
		<?php } if($this->session->error_msg){?>
		<div class="alert alert-success alert-dismissible fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<strong>Fail! </strong><?php echo $this->session->error_msg; ?></div>
		<?php } ?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Subscription Plan List
				</h1>
				<div class="btn btn-info import_btn" style="float:right;" data-toggle="modal" data-target="#addplan">Add Plan </div>
			</div>
			<!------------- Modal for Add Subscription Plan ------------------>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="addplan" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Add Subscription Plan <button style="float:right;" type="button" class="close" data-dismiss="modal" aria-label="Close">
		                      <span aria-hidden="true">×</span>
		                    </button></h5>
							
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/addSubscription">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Plan Name *</label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Plan name" class="col-xs-10 col-sm-5" name="plan_name" required="" />
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Plan Description *</label>

											<div class="col-sm-9">
												<textarea placeholder="Plan Description" class="col-xs-10 col-sm-5" name="plan_description" required=""></textarea>
											</div>
										</div>
										

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Plan Amount * </label>

											<div class="col-sm-9">
												<input type="number" id="form-field-1" placeholder="Plan Amount" class="col-xs-10 col-sm-5" name="plan_amount" required=""/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Status * </label>

											<div class="col-sm-9">
												<select class="col-xs-10 col-sm-5" id="form-field-select-1" name="plan_status" required="">
													<option value="">Select Status</option>
													<option value="1">Active</option>
													<option value="0">Inactive</option>
												</select>
											</div>
										</div>

										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
<!------------- Modal for Edit Subscription Plan ----------------------->
			<?php
				foreach($subscription_plan as $plan){ 
			?>
			<div class="modal fade" data-keyboard="false" data-backdrop="static" id="edit_plan<?=$plan['plan_id']?>" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title">Edit Subscription Plan <button style="float:right;" type="button" class="close" data-dismiss="modal" aria-label="Close">
		                      <span aria-hidden="true">×</span>
		                    </button></h5>
							
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<!-- PAGE CONTENT BEGINS -->
									<form class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>/admin/editSubscription/<?=$plan['plan_id']?>">
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Plan Name *</label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Plan name" class="col-xs-10 col-sm-5" name="plan_name" value="<?= $plan['plan_name']?>" required=""/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Plan Description *</label>

										<div class="col-sm-9">
												<textarea id="form-field-1" placeholder="Plan Description" class="col-xs-10 col-sm-5" name="plan_description" required=""><?= $plan['plan_details']?></textarea>
											</div>
											</div>
											

										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Plan Amount * </label>

											<div class="col-sm-9">
												<input type="text" id="form-field-1" placeholder="Plan Amount" class="col-xs-10 col-sm-5" name="plan_amount" required="" value="<?= $plan['plan_amount']?>"  readonly />
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Status * </label>

											<div class="col-sm-9">
												<select class="col-xs-10 col-sm-5" id="form-field-select-1" name="plan_status" required="">
													<option value="">Select Status</option>
													<option <?php echo ($plan['plan_status'] == 1) ? 'selected' : ''; ?> value="1">Active</option>
													<option <?php echo ($plan['plan_status'] == 0) ? 'selected' : ''; ?> value="0">Inactive</option>
												</select>
											</div>
										</div>

										<div class="space-4"></div>
										<div class="clearfix form-actions">
											<div class="col-md-offset-3 col-md-9">
												<button class="btn btn-info" type="submit">
													<i class="ace-icon fa fa-check bigger-110"></i>
													Submit
												</button>
												<!-- 
												&nbsp; &nbsp; &nbsp;
												<button class="submit" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button> -->
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php }?>
<!------------------------- Subscription Plan List ------------------------------>
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Plan Name</th>
								<th scope="col">Plan Description</th>
								<th scope="col">Plan Amount</th>
								<th scope="col">Plan Status</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>

							<?php 
							$snum = 0;
							foreach($subscription_plan as $plan){ 
								$snum += 1;
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td><?= $plan['plan_name']?></td>
								<td><?= $plan['plan_details']?></td>
								<td><?= $plan['plan_amount']?></td>
								<td>
									<?php if($plan['plan_status'] == 0){?>
										<span class="label label-sm label-danger">Inactive</span>
									<?php } else{ ?>
										<span class="label label-sm label-success">Active</span>
									<?php } ?>
										
								</td>
								<td>
									<span class="green">
										<i class="ace-icon fa fa-pencil-square-o bigger-120" data-toggle="modal" data-target="#edit_plan<?=$plan['plan_id']?>"></i>

									</span>
									<a href="<?php base_url(); ?>deleteSubscription/<?=$plan['plan_id']?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
