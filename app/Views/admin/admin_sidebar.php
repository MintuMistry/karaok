<ul class="nav nav-list">
	<li class="active">
		<a href="<?php echo base_url(); ?>">
			<i class="menu-icon fa fa-tachometer" style ="position:absolute; top:28% ! important; transform:translateY(-0%) !important;"></i>
			<span class="menu-text"> Dashboard </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/user_management">
			<i class="menu-icon fa fa-users" style ="position:absolute; top:28% ! important; transform:translateY(-0%) !important;"></i>
			<span class="menu-text"> User Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/subscription_management">
			<i class="menu-icon fa fa-users" style ="position:absolute; top:28% ! important; transform:translateY(-0%) !important;"></i>
			<span class="menu-text"> Subscription Plan </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/content_management">
			<i class="menu-icon glyphicon glyphicon-list" style ="position:absolute; top:28% ! important; transform:translateY(-0%) !important;"></i>
			<span class="menu-text"> Content Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/transaction_management">
			<i class="menu-icon glyphicon glyphicon-list" style ="position:absolute; top:28% ! important; transform:translateY(-0%) !important;"></i>
			<span class="menu-text"> Transaction Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/post_management">
			<i class="menu-icon glyphicon glyphicon-list" style ="position:absolute; top:28% ! important; transform:translateY(-0%) !important;"></i>
			<span class="menu-text"> Post Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/music_category">
			<i class="menu-icon glyphicon glyphicon-list" style ="position:absolute; top:28% ! important; transform:translateY(-0%) !important;"></i>
			<span class="menu-text"> Music Category </span>
		</a>
		<b class="arrow"></b>
	</li>
	
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/song_management">
			<i class="menu-icon glyphicon glyphicon-list" style ="position:absolute; top:28% ! important; transform:translateY(-0%) !important;"></i>
			<span class="menu-text"> Song Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/contact_management">
			<i class="menu-icon glyphicon glyphicon-list" style ="position:absolute; top:28% ! important; transform:translateY(-0%) !important;"></i>
			<span class="menu-text"> Contact Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	<li class="">
		<a href="<?php echo base_url(); ?>/admin/user_report_post_management">
			<i class="menu-icon glyphicon glyphicon-list" style ="position:absolute; top:28% ! important; transform:translateY(-0%) !important;"></i>
			<span class="menu-text"> Report Post Management </span>
		</a>
		<b class="arrow"></b>
	</li>
	
	<!--<li class="">
		<a href="<?php echo base_url(); ?>/admin/report_user_management">
			<i class="menu-icon glyphicon glyphicon-list" style ="position:absolute; top:28% ! important; transform:translateY(-0%) !important;"></i>
			<span class="menu-text"> Report User Management </span>
		</a>
		<b class="arrow"></b>
	</li>-->
</ul><!-- /.nav-list -->