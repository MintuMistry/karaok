<?php 
use App\Models\Users;
$users = new Users();

?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/report_user_management">Report User Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>

<style>
#toast-container > .toast-success {
    margin-left: auto !important;
    margin-top: 0px !important;
    top: 50px!important;
    right:20px;
}
.toast-close-button{
	position: absolute !important;
	right: 6px !important;
	top:6px !important;
}
.toast-message{
	color: #fff !important;
	padding :15px 6px !important;
}
</style>

		<?php $this->session = \Config\Services::session()?>
		<?php if($this->session->success_msg){ ?>
			<div class="alert alert-success alert-dismissible fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<strong>Success! </strong><?php echo $this->session->success_msg; ?></div>
		<?php } if($this->session->error_msg){?>
		<div class="alert alert-success alert-dismissible fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<strong>Fail! </strong><?php echo $this->session->error_msg; ?></div>
		<?php } ?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<div class="page-content">
			<div class="page-header">
				<h1>
					User Reports List
				</h1>
			</div>
			<!----------- user List ------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Reported By</th>
								<th scope="col">Reported User</th>
								<th scope="col">Comment</th>
								<th scope="col">Date</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$snum = 0;
							foreach($report_user_details as $report_user){ 
								$snum += 1;
								$reported_by = $users->crud_read($report_user['userid']);
								$reported_to = $users->crud_read($report_user['reported_userid']);
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td>
									<?php
										if($reported_by){
											echo $reported_by[0]['name'];
										}
										else {
											echo "";
										}
									?>
								</td>
								
								<td>
									<?php
										if($reported_to){
											echo $reported_to[0]['name'];
										}
										else {
											echo "";
										}
									?>
								</td>
								
								<td>
									<?php
										if($report_user['report_msg']){
											echo $report_user['report_msg'];
										}
										else {
											echo "No Comment";
										}
									?>
								</td>
								
								<td><?= date('m-d-Y', strtotime($report_user['date_added']))?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
