<?php
use App\Models\User_report;
$user_report = new User_report();
?>

<style>
	#toast-container > .toast-success {
	    margin-left: auto !important;
	    margin-top: 0px !important;
	    top: 50px!important;
	    right:40%;
	}
	.toast-close-button{
		position: absolute !important;
		right: 6px !important;
		top:6px !important;
	}
	.toast-message{
		color: #fff !important;
		padding :15px 6px !important;
	}
	.ms-options>ul>li {
		list-style-type: none;
		margin-bottom: 4px;
	}
	.ms-options>ul {
		margin: 0;
	}
	.ms-options>ul>li>label>input[type=checkbox] {
		margin-right: 6px;
	}
	.ms-options-wrap {
		border: 1px solid #e4e4e4;
		border-radius: 4px;
	}
	.ms-options-wrap>button {
		width: 100%;
		border: 0;
		border-bottom: 1px solid #dedede;
		padding: 10px 6px;
	}
	.input-group{
		position: absolute !important;
	}
	.main-content {
	    margin-left: 190px;
	}
</style>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/user_management">User Management</a>
				</li>
			</ul>
		</div>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<?php $this->session = \Config\Services::session()?>
		<?php if($this->session->success_msg){ ?>
			<div class="alert alert-success alert-dismissible fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
				<strong>Success! </strong><?php echo $this->session->success_msg; ?>
			</div>
		<?php } if($this->session->error_msg){?>
			<div class="alert alert-success alert-dismissible fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
				<strong>Fail! </strong><?php echo $this->session->error_msg; ?>
			</div>
		<?php } ?>
		<div class="page-content">
			<div class="page-header">
				<h1>
					User List
				</h1>
			</div>
			
			<!--------------------- User List ---------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">Profile Image</th>
								<th scope="col">User Name</th>
								<th scope="col">Full Name</th>
								<th scope="col">Email</th>
								<th scope="col">Mobile</th>
								<th scope="col">Bio</th>
								<th scope="col">Block</th>
								<th scope="col">Reported By</th>
								<th scope="col">View Notifications</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$snum = 0;
							foreach($user_details as $user){ 
								$snum += 1;
								$report_user_data = count($user_report->crud_read($user['user_id']));
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td>
								<?php 
								if(!empty($user['profile_img']))
								{?>
								<img src="<?php echo base_url()."/writable/uploads/".$user['profile_img']?>" height="100px" width="100px" alt="Profile">
								<?php
								}else{?>
								
								<img src="<?php echo base_url()."/public/assets/images/avatars/avatar.png"?>" height="100px" width="100px" alt="Profile">
								<?php								
								}
								
								?>
								</td>
								<td><?= $user['user_name']?></td>
								<td><?= $user['name'] ?></td>
								<td><?= $user['user_email']?></td>
								<td><?= $user['mobile']?></td>
								<td><?= $user['bio']?></td>
								<td style="width: 75px;">
									<?php if($user['is_active'] == 0){?>
										<span class="label label-sm label-danger" ><a href="<?=base_url('admin')?>/blockUser/<?=$user['user_id']?>" style="color:#FFF;padding-left: 21px; padding-right: 21px;">Unblock
										</a></span>
									<?php } else{ ?>
									<span class="label label-sm label-success" ><a href="<?=base_url('admin')?>/blockUser/<?=$user['user_id']?>" style="color:#FFF;padding-left: 21px; padding-right: 21px;">Block</span>
										</a>
									<?php } ?>
								</td>
								<td>
									<?php 
										if($report_user_data > 0){ ?>
											<a href="<?php base_url(); ?>report_user_management/<?=$user['user_id']?>">
												<span style="margin-left: 40px;"><?php echo $report_user_data;?></span>
											</a>
									<?php }
										else {?>
											<span style="margin-left: 40px;"><?php echo $report_user_data;?></span>
										<?php }
									?>
								</td>
								<td>
									<a href="<?php base_url(); ?>notification_management/<?=$user['user_id']?>">
										<i class="ace-icon fa fa-flag bigger-120" style="margin-left: 50px;"></i>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>/assets/js/multiselect.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" >
  	$(function () {
		$('.datepick').datepicker({
			format: 'yyyy-mm-dd',
			orientation: 'bottom'
		});
	});
 $(window).load(function() {
    $('#multiinterest1').multiselect({
});
});
$('#multiinterest').multiselect({
});
function validate(key)
{

//getting key code of pressed key

var keycode = (key.which) ? key.which : key.keyCode;

var phn = document.getElementById('txtPhn');

//comparing pressed keycodes

if (!(keycode==8 || keycode==46)&&(keycode < 48 || keycode > 57))
{
	return false;
}
else
{
//Condition to check textbox contains ten numbers or not
if (phn.value.length <10)
{
	return true;
}
else
{
	return false;
}
}
}



// Get references to the "add more" button and the file input

var addMore = document.querySelector('.add-more')

var fileInput = document.querySelector('.file-input')

	var count = 0;

// When the button got clicked...



addMore.addEventListener('click', function (event) {

	checkcount(count);

  // Prevent the default action, which would be

  // the form submission for a button

  event.preventDefault()

  

  // Create a clone of the file input (you could also

  // create a new one from scrath, but then you'd

  // have to manually add the attributes and all...)

  //var newFileInput = fileInput.cloneNode()

  var newFileInput = fileInput.cloneNode()

		newFileInput.value = '';

  

  // Insert the clone right after the original (this

  // can only be done inderectly)

  fileInput.parentNode.insertBefore(

  	newFileInput,

    fileInput.nextSibling

  )

  fileInput = newFileInput

  count++;

})


function checkcount(count)
{
	if(count >=4 )
	{
		$(".add-more").hide();
	}
}
</script>

