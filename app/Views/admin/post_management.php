<?php 
use App\Models\Users;
use App\Models\Songs;
$users = new Users();
?>
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="<?=base_url()?>">Home</a>
				</li>
				<li class="active">
					<a class="pages_link" href="<?=base_url('admin')?>/post_management">Post Management</a>
				</li>
			</ul><!-- /.breadcrumb -->
		</div>

<style>
#toast-container > .toast-success {
    margin-left: auto !important;
    margin-top: 0px !important;
    top: 50px!important;
    right:20px;
}
.toast-close-button{
	position: absolute !important;
	right: 6px !important;
	top:6px !important;
}
.toast-message{
	color: #fff !important;
	padding :15px 6px !important;
}
</style>

		<?php $this->session = \Config\Services::session()?>
		<?php if($this->session->success_msg){ ?>
			<div class="alert alert-success alert-dismissible fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<strong>Success! </strong><?php echo $this->session->success_msg; ?></div>
		<?php } if($this->session->error_msg){?>
		<div class="alert alert-success alert-dismissible fade in">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
		<strong>Fail! </strong><?php echo $this->session->error_msg; ?></div>
		<?php } ?>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		</button>
		<div class="page-content">
			<div class="page-header">
				<h1>
					Posts List
				</h1>
			</div>
			<!----------- Posts List ------------------------->
			<div class="row">
				<div class="col-xs-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th scope="col">S.No</th>
								<th scope="col">User Name</th>
								<th scope="col">Song Category</th>
								<th scope="col">Song Name</th>
								<th scope="col">Video</th>
								<th scope="col">Song Artist</th>
								<th scope="col">Block Status</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$snum = 0;
							foreach($song_details as $post){ 
								$snum += 1;
								$user_data = $users->crud_read($post['user_id']);
								$Song = new Songs();
								//echo $song['music_cat_id'];
								
								$catname = $Song->curd_read_music_category($post['music_cat_id']);
							?>
							<tr>
								<th scope="row"><?= $snum?></th>
								<td>
									<?php
										if($user_data){
											echo $user_data[0]['name'];
										}
										else {
											echo "";
										}
									?>
								</td>
								<td><?= $catname[0]['category_name']?></td>
								<td><?= $post['song_name']?></td>
								<td>
								<?php 
								if($post['video_file'])
								{?>
								<video class="bg-video" style="height: 110px;width: 125px;" poster="<?php echo base_url()?>/assets/images/video_play.png" autoplay="" loop="" muted="">
										<source src="<?php echo base_url()."/writable/uploads/".$post['video_file']?>">
									</video>
								<?php	
								}else{
									echo "No Video";									
								}
								?>
								</td>
								<td><?= $post['artist_name']?></td>
								<td>
									<?php 
										if($post['video_status'] == 1){
									?>
										<span class="label label-sm label-danger" >
											<a href="<?=base_url('admin')?>/blockPost/<?=$post['video_id']?>" style="color:#FFF;padding-left: 21px; padding-right: 21px;">Unblock
											</a>
										</span>
									<?php } else { 
									?>
										<span class="label label-sm label-success" >
											<a href="<?=base_url('admin')?>/blockPost/<?=$post['video_id']?>" style="color:#FFF;padding-left: 21px; padding-right: 21px;">Block
											</a>
										</span>
									<?php } ?>
								</td>
								
								<td>
									<a href="<?php base_url(); ?>deletePost/<?=$post['video_id']?>" class="ace-icon fa fa-delete-o bigger-120">
										<span class="red">
											<i class="ace-icon fa fa-trash-o bigger-120"></i>
										</span>
									</a>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>