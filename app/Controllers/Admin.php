<?php 

namespace App\Controllers;

/*-----Load models-----*/
use App\Models\Login;
use App\Models\Users;
use App\Models\Content;
use App\Models\Subscription;
use App\Models\Transaction;
use App\Models\Posts;
use App\Models\Songs;
use App\Models\Notifications;
use App\Models\Contact;
use App\Models\User_report_post;
use App\Models\User_report;

use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\Files\UploadedFile;


class Admin extends BaseController

{
	public function __construct()
    {
        $this->session = session();
    }

	private function checkLogin()
	{
		if(!$this->session->get('admin_id')){
			header("location: ".base_url('admin')."/login");
		}
		else {
			return;
		}
	}

	private function loadView($viewname,$data = array())
	{
		echo view("admin/admin_header");
		echo view("admin/$viewname", $data);
		echo view("admin/admin_footer");
	}

	private function loadView1($viewname,$data = array())
	{
		echo view("admin/$viewname", $data);
	}

	public function index()
	{
		$this->checkLogin();
		$this->loadView("dashboard");	
	}

	public function login()
	{
		echo view("admin/login");
	}

	/*--------------------------Admin Management----------------------------*/
	public function login_check()
	{
		$login = new login();
		$admin_id = $login->admin_verify(
			$this->request->getPost('email'),
			$this->request->getPost('password')
		);
		if ($admin_id >0){
			$this->session->set('admin_id', $admin_id);
			$this->session->admin_data = $login->admin_data($admin_id);
		   	return redirect()->to(base_url('admin')); //dashboard
		}
		else {
			$data['msg'] = "Incorrect Email or Password";
			$this->loadView1("login", $data);
		}
	}

	public function changePassword()
	{
		$login = new login();
		$admin_id = $this->session->get('admin_id');
		$old = $this->request->getPost("oldpassword");
		$new = $this->request->getPost("password");
		$confirm = $this->request->getPost("cpassword");
		$admin_data = $login->admin_data($admin_id);
		if($old != $admin_data->password) {
			$this->session->setFlashdata('error_msg', "Old password does not match");
		}
		else {
			if($new == $confirm){
				$login->crud_update(
					array(
						"password" => $new
					), $admin_id
				);
				$this->session->setFlashdata('success_msg', "Password updated successfully");
			}
			elseif($admin_data->password == $new){
				$this->session->setFlashdata('error_msg', "Old and New password can not be same");
			}
			else {
				$this->session->setFlashdata('error_msg', "new and Confirm password should be same");
			}
		}
		return redirect()->to(base_url('admin/change_password_management'));
	}
	public function logout()
	{
		$this->session->remove('admin_id');
		$this->session->destroy();
		return redirect()->to(base_url('admin/login') );
	}

	/*--------------------------User Management----------------------------*/
	public function blockUser($user_id)
	{
		$this->checkLogin();
		$users = new Users();
		$res = $users->crud_read($user_id);
		if(!empty($res))
		{
			if($res[0]['is_active']==0)
			{
				$users->crud_update(
				array(
					"is_active" => 1,
				), $user_id);
				$this->session->setFlashdata('success_msg', "User unblocked successfully");
				return redirect()->to(base_url('admin/user_management'));
			} else {
				$users->crud_update(
				array(
					"is_active" => 0,
				), $user_id);
				$this->session->setFlashdata('success_msg', "User Blocked successfully");
			return redirect()->to(base_url('admin/user_management'));
			}
		} else
		{
			$this->session->setFlashdata('success_msg', "User not found");
			return redirect()->to(base_url('admin/user_management'));
		}	
	}	


	/*---------------Subscription Management----------------------------*/
	public function addSubscription()
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$subscription->crud_create(
			array(
				"plan_name" => $this->request->getPost("plan_name"),
				"plan_amount" => $this->request->getPost("plan_amount"),
				"plan_details" => $this->request->getPost("plan_description"),
				"plan_status" => $this->request->getPost("plan_status")
			)
		);
		$this->session->setFlashdata('success_msg', "Subscription added successfully");
		return redirect()->to(base_url('admin/subscription_management'));
	}

	public function editSubscription($plan_id)
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$subscription->crud_update(
			array(
				"plan_name" => $this->request->getPost("plan_name"),
				"plan_amount" => $this->request->getPost("plan_amount"),
				"plan_details" => $this->request->getPost("plan_description"),
				"plan_status" => $this->request->getPost("plan_status")
			), $plan_id
		);
		/* echo $subscription->getLastQuery();
		die(); */
		$this->session->setFlashdata('success_msg', "Subscription updated successfully");
		return redirect()->to(base_url('admin/subscription_management'));
	}

	public function deleteSubscription($plan_id)
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$subscription->crud_delete($plan_id);
		$this->session->setFlashdata('success_msg', "Subscription deleted successfully");
		return redirect()->to(base_url('admin/subscription_management'));
	}

	/*----------------------Content Management----------------------------*/
	public function editContent($content_id)
	{
		$this->checkLogin();
		$content = new Content();
		$content->crud_update(
			array(
				"page_content" => $this->request->getPost("page_content")
			), $content_id
		);
		$this->session->setFlashdata('success_msg', "Content updated successfully");
		return redirect()->to(base_url('admin/content_management'));
	}

	public function deleteContent($content_id)
	{
		$this->checkLogin();
		$content = new Content();
		$content->crud_delete($content_id);
		$this->session->setFlashdata('success_msg', "Content deleted successfully");
		return redirect()->to(base_url('admin/content_management'));
	}

	/*--------------------------Post Management----------------------------*/
	public function blockPost($videoid)
	{
		$this->checkLogin();
		$posts = new Posts();
		$songs = new Songs();
		$post_details = $songs->crud_read_video($videoid);
		if($post_details[0]['video_status']==0) {
			$songs->crud_update_video(
			array(
				"video_status" => 1,
			), $videoid);
			$this->session->setFlashdata('success_msg', "Post Blocked successfully");
			return redirect()->to(base_url('admin/post_management'));
		} else {
			$songs->crud_update_video(
			array(
				"video_status" => 0,
			), $videoid);
			$this->session->setFlashdata('success_msg', "Post UnBlocked successfully");
		return redirect()->to(base_url('admin/post_management'));
		}
	}

	public function reportPost($post_id)
	{
		$this->checkLogin();
		$posts = new Posts();
		$post_details = $posts->crud_read($post_id);
		if($post_details[0]['report_status']==0) {
			$posts->crud_update(
			array(
				"report_status" => 1,
			), $post_id);
			$this->session->setFlashdata('success_msg', "Post unblocked successfully");
			return redirect()->to(base_url('admin/post_management'));
		} else {
			$posts->crud_update(
			array(
				"report_status" => 0,
			), $post_id);
			$this->session->setFlashdata('success_msg', "Post Blocked successfully");
		return redirect()->to(base_url('admin/post_management'));
		}
	}

	public function deletePost($song_id)
	{
		$this->checkLogin();
		$songs = new Songs();
		$songs->crud_delete($song_id);
		$this->session->setFlashdata('success_msg', "Post deleted successfully");
		return redirect()->to(base_url('admin/post_management'));
	}

	/*---------------Song Management----------------------------*/
	public function addSong()
	{
		$this->checkLogin();
		$songs = new Songs();
		$sound_track = $this->request->getFile("song_track");
		$song_image = $this->request->getFile("song_image");
		$date =date('Y-m-d H:i:s');
		if($sound_track->isValid()){
			$sound_trackpath = $sound_track->store();
			if($sound_trackpath != ""){
				$song_id = $songs->upload_video(
					array(
						'user_id' => 1,
						'video_file' => $sound_trackpath,
						'music_cat_id' => $this->request->getPost("song_category"),
						'artist_name' => $this->request->getPost("song_artist"),
						'song_name' => $this->request->getPost("song_name"),
						'video_thumb' => $photopath1,
						'created_on' => $date
					)
				);
				if($song_image->isValid()){
					$sound_imagepath = $song_image->store();
					if($sound_imagepath != ""){
						$songs->crud_update_video(
							array(
								"video_thumb" => $sound_imagepath,"old_video_id" => $song_id,"main_video_id" => $song_id
							), $song_id
						);
						
					}
				}
			}
		}
		$this->session->setFlashdata('success_msg', "Song added successfully");
		return redirect()->to(base_url('admin/song_management'));
	}

	public function editSong($song_id)
	{
		$this->checkLogin();
		$songs = new Songs();
		//print_r($_POST);die;
		$sound_track = $this->request->getFile("song_track");
		$song_image = $this->request->getFile("song_image");
		if ($song_image->isValid() && $sound_track->isValid()) {
			$sound_imagepath = $song_image->store();
			if($sound_imagepath != ""){
				$songs->crud_update(
					array(
						"song_name" => $this->request->getPost("song_name"),
						"music_cat_id" => $this->request->getPost("song_category"),
						"song_artist" => $this->request->getPost("song_artist"),
						"song_genre" => $this->request->getPost("song_genre"),
						"song_thumbnail" => $sound_imagepath
					), $song_id
				);
			}
			$sound_trackpath = $sound_track->store();
			if($sound_trackpath != ""){
				$songs->crud_update(
					array(
						"song_name" => $this->request->getPost("song_name"),
						"music_cat_id" => $this->request->getPost("song_category"),
						"song_track" => $sound_trackpath,
						"song_artist" => $this->request->getPost("song_artist"),
						"song_genre" => $this->request->getPost("song_genre")
					), $song_id
				);
			}
		}
		elseif($sound_track->isValid()){
			$sound_trackpath = $sound_track->store();
			if($sound_trackpath != ""){
				$songs->crud_update(
					array(
						"song_name" => $this->request->getPost("song_name"),
						"music_cat_id" => $this->request->getPost("song_category"),
						"song_track" => $sound_trackpath,
						"song_artist" => $this->request->getPost("song_artist"),
						"song_genre" => $this->request->getPost("song_genre")
					), $song_id
				);
			}
		}
		elseif($song_image->isValid()){
			$sound_imagepath = $song_image->store();
			if($sound_imagepath != ""){
				$songs->crud_update(
					array(
						"song_name" => $this->request->getPost("song_name"),
						"music_cat_id" => $this->request->getPost("song_category"),
						"song_artist" => $this->request->getPost("song_artist"),
						"song_genre" => $this->request->getPost("song_genre"),
						"song_thumbnail" => $sound_imagepath
					), $song_id
				);
			}
		}
		else {
			$songs->crud_update(
				array(
					"song_name" => $this->request->getPost("song_name"),
					"music_cat_id" => $this->request->getPost("song_category"),
					"song_artist" => $this->request->getPost("song_artist"),
					"song_genre" => $this->request->getPost("song_genre")
				), $song_id
			);
		}
		$this->session->setFlashdata('success_msg', "Song updated successfully");
		return redirect()->to(base_url('admin/song_management'));
	}

	public function deleteSong($song_id)
	{
		$this->checkLogin();
		$songs = new Songs();
		$songs->crud_delete($song_id);
		$this->session->setFlashdata('success_msg', "Song deleted successfully");
		return redirect()->to(base_url('admin/song_management'));
	}

	/*--------------- Notification Management----------------------------*/
	public function addNotification()
	{
		$this->checkLogin();
		$notifications = new Notifications();
		//print_r($_POST);die;
		$notifications->crud_create(
			array(
				"userid" => $this->request->getPost("user_id"),
				"message" => $this->request->getPost("message"),
				"notification_status" => 1,
				"created_at" => date('Y-m-d H:i:s')
			)
		);
		//echo $notifications->getLastQuery();die;
		$this->session->setFlashdata('success_msg', "Notification sent successfully");
		return redirect()->to(base_url('admin/notification_management/'.$this->request->getPost("user_id")));
	}

	/*--------------- User Report Post Management----------------------------*/
	public function userReportPost($user_report_post_id)
	{
		$this->checkLogin();
		$user_report_post = new User_report_post();
		$post_details = $user_report_post->crud_read($user_report_post_id);
		if($post_details[0]['report_status']==0) {
			$user_report_post->crud_update(
			array(
				"report_status" => 1,
			), $user_report_post_id);
			$this->session->setFlashdata('success_msg', "Post unblocked successfully");
			return redirect()->to(base_url('admin/user_report_post_management'));
		} else {
			$user_report_post->crud_update(
			array(
				"report_status" => 0,
			), $user_report_post_id);
			$this->session->setFlashdata('success_msg', "Post Blocked successfully");
		return redirect()->to(base_url('admin/user_report_post_management'));
		}
	}

	public function deleteReportPost($post_report_id)
	{
		$this->checkLogin();
		$user_report_post = new User_report_post();
		$user_report_post->crud_delete($post_report_id);
		$this->session->setFlashdata('success_msg', "Report Post deleted successfully");
		return redirect()->to(base_url('admin/user_report_post_management'));
	}
	
	
		/*---------------Subscription Management----------------------------*/
	public function addCategory()
	{
		$this->checkLogin();
		$songs = new Songs();
		$cat_image = $this->request->getFile("cat_image");
		$cat_imagepath ="";
		if($cat_image->isValid()){
			$cat_imagepath = $cat_image->store();		
		}
				
		$cat_id = $songs->crud_create_music_category(
			array(
				"category_name" => $this->request->getPost("category_name"),
				"category_img" => $cat_imagepath
			)
		);
		
		
		$this->session->setFlashdata('success_msg', "Music Category added successfully");
		return redirect()->to(base_url('admin/music_category'));
	}

	public function editCategory($cat_id)
	{
		$this->checkLogin();
		$songs = new Songs();
		$cat_image = $this->request->getFile("cat_image");
		$cat_imagepath ="";
		if($cat_image->isValid()){
			$cat_imagepath = $cat_image->store();		
		}
		if($cat_imagepath == "")
		{
			$songs->crud_update_music_category(
			array(
				"category_name" => $this->request->getPost("category_name")
			), $cat_id
			);
		}else{
			$songs->crud_update_music_category(
			array(
				"category_name" => $this->request->getPost("category_name"),
				"category_img" => $cat_imagepath
			), $cat_id
			);
		}
		
		$this->session->setFlashdata('success_msg', "Music Category updated successfully");
		return redirect()->to(base_url('admin/music_category'));
	}

	public function deleteCategory($cat_id)
	{
		$this->checkLogin();
		$songs = new Songs();
		$songs->crud_delete_music_category($cat_id);
		$this->session->setFlashdata('success_msg', "Music Category deleted successfully");
		return redirect()->to(base_url('admin/music_category'));
	}
	
	/*--------------------------Load Views----------------------------*/
	
	public function music_category()
	{
		$this->checkLogin();
		$songs = new Songs();
		$data['music_category'] = $songs->curd_read_music_category();
		$this->loadView("music_category", $data);
	}
	
	public function user_management()
	{
		ini_set('max_execution_time',0);
		$this->checkLogin();
		$users = new Users();
		$data['user_details'] = $users->crud_read();
		$this->loadView("user_management", $data);
	}

	public function subscription_management()
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$data['subscription_plan'] = $subscription->crud_read();
		$this->loadView("subscription_plan", $data);
	}

	public function content_management()
	{
		$this->checkLogin();
		$content = new Content();
		$data['contents'] = $content->crud_read();
		$this->loadView("content", $data);
	}

	public function transaction_management()
	{
		$this->checkLogin();
		$transaction = new Transaction();
		$data['transaction_details'] = $transaction->crud_read();
		$this->loadView("transactions", $data);
	}

	public function post_management()
	{
		$this->checkLogin();
		$songs = new Songs();
		$data['song_details'] = $songs->crud_read_video();
		$data['music_category'] = $songs->curd_read_music_category();
		$this->loadView("post_management", $data);
	}

	public function change_password_management()
	{
		$this->checkLogin();
		$this->loadView("change_password");
	}

	public function song_management()
	{
		$this->checkLogin();
		$songs = new Songs();
		$userid =1;
		$data['song_details'] = $songs->crud_read_user_video($userid);
		$data['music_category'] = $songs->curd_read_music_category();
		$this->loadView("song_management", $data);
	}

	public function notification_management($user_id)
	{
		$this->checkLogin();
		$notifications = new Notifications();
		$data['user_id'] = $user_id;
		$data['notifications_details'] = $notifications->read_user_notifications($user_id);
		$this->loadView("notification_management", $data);
	}

	public function contact_management()
	{
		$this->checkLogin();
		$contact = new Contact();
		$data['contact_details'] = $contact->crud_read();
		$this->loadView("contact_management", $data);
	}

	public function user_report_post_management()
	{
		$this->checkLogin();
		$user_report_post = new User_report_post();
		$data['report_post_details'] = $user_report_post->crud_read();
		$this->loadView("report_post_management", $data);
	}

	public function report_user_management($user_id)
	{
		$this->checkLogin();
		$user_report = new User_report();
		$data['user_id'] = $user_id;
		$data['report_user_details'] = $user_report->crud_read($user_id);
		$this->loadView("report_user_management", $data);
	}

}

