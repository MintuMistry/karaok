<?php 

namespace App\Controllers;

/*-----Load models-----*/
use App\Models\Login;
use App\Models\Users;
use App\Models\Users1;
use App\Models\Content;
use App\Models\Subscription;
use App\Models\Transaction;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\Files\UploadedFile;


class Admin1 extends BaseController

{
	public function __construct()
    {
        $this->session = session();
    }

	private function checkLogin()
	{
		if(!$this->session->get('admin_id')){
			header("location: ".base_url('admin')."/login");
		}
		else {
			return;
		}
	}

	private function loadView($viewname,$data = array())
	{
		echo view("admin/admin_header");
		echo view("admin/$viewname", $data);
		echo view("admin/admin_footer");
	}

	private function loadView1($viewname,$data = array())
	{
		echo view("admin/$viewname", $data);
	}

	public function index()
	{
		$this->checkLogin();
		$this->loadView("dashboard");	
	}

	public function login()
	{
		echo view("admin/login");
	}

	public function login_check()
	{
		$login = new login();
		$admin_id = $login->admin_verify(
			$this->request->getPost('email'),
			$this->request->getPost('password')
		);
		if ($admin_id >0){
			$this->session->set('admin_id', $admin_id);
			$this->session->admin_data = $login->admin_data($admin_id);
		   	return redirect()->to(base_url('admin')); //dashboard
		}
		else {
			$data['msg'] = "Incorrect Email or Password";
			$this->loadView1("login", $data);
		}
	}

	public function logout()
	{
		$this->session->remove('admin_id');
		$this->session->destroy();
		return redirect()->to(base_url('admin/login') );
	}

	/*--------------------------User Management----------------------------*/
	public function blockUser($user_id)
	{
		$this->checkLogin();
		$users = new Users();
		$res = $users->crud_read($user_id);
		if(!empty($res))
		{
			if($res[0]['is_active']==0)
			{
				$users->crud_update(
				array(
					"is_active" => 1,
				), $user_id);
				$this->session->setFlashdata('success_msg', "User unblocked successfully");
				return redirect()->to(base_url('admin/user_management'));
			} else {
				$users->crud_update(
				array(
					"is_active" => 0,
				), $user_id);
				$this->session->setFlashdata('success_msg', "User Blocked successfully");
			return redirect()->to(base_url('admin/user_management'));
			}
		} else
		{
			$this->session->setFlashdata('success_msg', "User not found");
			return redirect()->to(base_url('admin/user_management'));
		}	
	}	


	/*---------------Subscription Management----------------------------*/

	public function addSubscription()
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$subscription->crud_create(
			array(
				"plan_name" => $this->request->getPost("plan_name"),
				"plan_amount" => $this->request->getPost("plan_amount"),
				"plan_desc" => $this->request->getPost("plan_description"),
				"plan_status" => $this->request->getPost("plan_status")
			)
		);
		$this->session->setFlashdata('success_msg', "Subscription added successfully");
		return redirect()->to(base_url('admin/subscription_management'));
	}

	public function editSubscription($plan_id)
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$subscription->crud_update(
			array(
				"plan_name" => $this->request->getPost("plan_name"),
				"plan_amount" => $this->request->getPost("plan_amount"),
				"plan_desc" => $this->request->getPost("plan_description"),
				"plan_status" => $this->request->getPost("plan_status")
			), $plan_id
		);
		$this->session->setFlashdata('success_msg', "Subscription updated successfully");
		return redirect()->to(base_url('admin/subscription_management'));
	}

	public function deleteSubscription($plan_id)
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$subscription->crud_delete($plan_id);
		$this->session->setFlashdata('success_msg', "Subscription deleted successfully");
		return redirect()->to(base_url('admin/subscription_management'));
	}

	/*--------------------------Content Management----------------------------*/
	public function editContent($content_id)
	{
		$this->checkLogin();
		$content = new Content();
		$content->crud_update(
			array(
				"page_name" => $this->request->getPost("page_name"),
				"page_content" => $this->request->getPost("page_content")
			), $content_id
		);
		$this->session->setFlashdata('success_msg', "Content updated successfully");
		return redirect()->to(base_url('admin/content_management'));
	}

	public function deleteContent($content_id)
	{
		$this->checkLogin();
		$content = new Content();
		$content->crud_delete($content_id);
		$this->session->setFlashdata('success_msg', "Content deleted successfully");
		return redirect()->to(base_url('admin/content_management'));
	}



	/*--------------------------Load Views----------------------------*/
	public function user_management()
	{
		ini_set('max_execution_time',0);
		$this->checkLogin();
		$users = new Users();
		$data['user_details'] = $users->crud_read();
		$this->loadView("user", $data);
	}

	public function subscription_management()
	{
		$this->checkLogin();
		$subscription = new Subscription();
		$data['subscription_plan'] = $subscription->crud_read();
		$this->loadView("subscription_plan", $data);
	}

	public function content_management()
	{
		$this->checkLogin();
		$content = new Content();
		$data['contents'] = $content->crud_read();
		$this->loadView("content", $data);
	}

	public function transaction_management()
	{
		$this->checkLogin();
		$transaction = new Transaction();
		$data['transaction_details'] = $transaction->crud_read();
		$this->loadView("transactions", $data);
	}
	
	public function report_post_management()
	{
		$this->checkLogin();
		$users = new Users1();
		$data['transaction_details'] = $users->crud_read();
		$this->loadView("transactions", $data);
	}


}

