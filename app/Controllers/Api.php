<?php 

/*

	Karaoke API controller

*/

namespace App\Controllers;

use Twilio\Rest\Client;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\Files\UploadedFile;

use App\Models\Users;
use App\Models\Content;
use App\Models\Subscription;
use App\Models\Block_unblock;
use App\Models\Songs;
use App\Models\Notifications;
use App\Models\Contact;

class Api extends BaseController

{
	//protected $upload;
	use ResponseTrait;
	private $TWILIO_SID = 'ACb77064a9aeb8c2da4018f0b97a742f04';
	private	$TWILIO_TOKEN = '37be8ca298ce8ac812a5140e99d29421';
	private function apiResponse($data)
	{
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	private function generateOTP()
	{
		$otp = "";
      	$generator = "1357902468";
      	for ($i = 1; $i <= 4; $i++) { 
	        $otp .= substr($generator, (rand()%(strlen($generator))), 1); 
	    } 
	    return $otp;
	}

	public function index()
	{
		$returndata = array(
			"message" => "Karaoke API"
		);
		return $this->respond($returndata,200);
	}

	private function sendTwilioSMS($text,$to)
	{
		try{
			$twilio = new Client($this->TWILIO_SID, $this->TWILIO_TOKEN);
	        return $twilio->messages->create(
			    $to,
			    [
			        'from' => '+12673949525',
			        'body' => $text
			    ]
			);
		}
		catch(Exception $e){
			return 0;
		}
		finally {
			return 0;
		}
	}
	
	Public function send_otp()
	{
		$returndata = array();
		$mobile = $this->request->getPost("mobile");
		$email = $this->request->getPost("email");
		$users = new Users();
		if(!empty($mobile) || !empty($email)){
			$userdata = $users->read_by_mail_or_phone($email,$mobile);
			$code = $this->generateOTP();
			$msg = "OTP for Karaoke, Your OTP is ".$code;
			if($email != ""){
				$send_status = mail($email, "Karaoke OTP", $msg, array("from" => "no-reply@mindfreeze.yilstaging.com"));
			}
			elseif($mobile != ""){
				$mobile = '+1'.$mobile;
				$resp = $this->sendTwilioSMS($msg,$mobile);
			}
			$returndata['status'] = true;
			$returndata['otp'] = $code;
			if(!empty($userdata)){
				$returndata['user_id'] = $userdata['user_id'];
				//$users->crud_update(array(
				//"fcmtoken" => $this->request->getPost("fcm_token")),$userdata[0]['user_id']); 
				if($userdata['is_active'] != 1)
				{
					$returndata['message'] = "Inactive User!";
				}
			}
		}
        return $this->apiResponse($returndata);
	}
	
	private function sendMail($to,$subject,$message)
	{
		mail($to,$subject,$message,array("from" => "no-reply@caviar.yesitlabs.xyz"));
	}

	public function register_user()
	{
		$returndata = array();
		$users = new Users();
		$name = $this->request->getPost("name");
		$user_name = $this->request->getPost("user_name");
		$mobile = $this->request->getPost("mobile");
		$email = $this->request->getPost("email");
		$password = $this->request->getPost("password");
		$fcm_token = $this->request->getPost('fcm_token');
		$bio = $this->request->getPost("bio");
		$user_image = $this->request->getFile("user_image");
		$users = new Users();
		if((!empty($mobile) || !empty($email)) && !empty($password) && !empty($name) && !empty($user_name))
		{
			$check_email = $users->check_email_exists($email);
			if($check_email > 0)
			{
				$user_id = $check_email;
			}else{
				
				if($user_image){
					if($user_image->isValid()){
						$photopath = $user_image->store();
						if($photopath != ""){
							$user_id = $users->crud_create(array(
								'user_name' => $user_name,
								'name' => $name,
								'user_email' => $email,
								'mobile' => $mobile,
								'password' => $password,
								'fcmtoken' => $fcm_token,
								'is_active' => 1,
								'bio' => $bio,
								'profile_img' => $photopath 
							));
						}
					}
				}
				else {
					$user_id = $users->crud_create(array(
						'user_name' => $user_name,
						'name' => $name,
						'user_email' => $email,
						'mobile' => $mobile,
						'password' => $password,
						'fcmtoken' => $fcm_token,
						'is_active' => 1,
						'bio' => $bio
					));
				}
			}
			$returndata['status'] = "success";
			$returndata['user_id'] = $user_id;
        	$returndata['message'] = "User registered successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}

	public function check_email()
	{
		$returndata = array();
		$users = new Users();
		$email = $this->request->getPost("email");
		if(!empty($email)){
			$user_data = $users->read_by_mail($email);
			if(!empty($user_data)){
				$returndata['status'] = "success";
        		$returndata['message'] = "Email already registered!";
			}
			else{
				$returndata['status'] = "success";
        		$returndata['message'] = "Email not registered!";
			}
		}
		else {
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}

	public function login()
	{
		$returndata = array();
		$users = new Users();
		$user_name = $this->request->getPost("user_name");
		$email = $this->request->getPost("email");
		$password = $this->request->getPost("password");
		$fcm_token = $this->request->getPost('fcm_token');
		if((!empty($user_name) || !empty($email)) && !empty($password))
		{
			$userdata = $users->read_by_mail_or_username($email, $user_name, $password);
			if(count($userdata) < 1){
				$returndata['status'] = "success";
	        	$returndata['message'] = "Invalid User!";
			}
			else{
				$userdata = $userdata[0];
				if($userdata['is_active'] == 0){
					$returndata['status'] = "success";
		        	$returndata['message'] = "User is inactive";
				}
				else{
					$users->crud_update(array('fcmtoken'=> $fcm_token), $userdata['user_id']);
					$returndata['status'] = "success";
					$returndata['user_id'] = $userdata['user_id'];
		        	$returndata['message'] = "Logged in successfully!";
				}
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}

	public function forget_password() 
    {
    	$returndata = array();
        $email = $this->request->getPost("email");
        $mobile = $this->request->getPost("mobile");
        $users = new Users();
        if(!empty($email) || !empty($mobile)){
            $data = $users->read_by_mail_or_phone($email, $mobile);
            //echo $users->getLastQuery();
            if(!empty($data)){   
                $otp = $this->generateOTP();
                $message = 'Your Verification OTP Is :-'.$otp;
                if(!empty($email)){
                	$send_status = mail($email, "Forget Password", $message, array("from" => "no-reply@karaoke.yesitlabs.xyz"));
	                if ($send_status) {
	                    $returndata['status'] = "success";  
	                    $returndata['userid'] = $data[0]['user_id'];  
	                    $returndata['otp'] = $otp;                  
	                    $returndata['message'] = 'OTP is sent on provided email!' ;
	                } else {                               
	                    $returndata['status'] = "fail";                   
	                    $returndata['message'] = 'Sorry, there is some internal issue, please try again';
	                }
                }
                elseif(!empty($mobile)){
                	$result = $this->sendTwilioSMS($message, $mobile);
                	//print_r($result);
                	$returndata['status'] = "success";
					$returndata['userid'] = $data[0]['user_id']; 
                    $returndata['otp'] = $otp;                  
                    $returndata['message'] = 'OTP is sent on provided mobile number!' ;
                }
            }
            else {
                $returndata['status'] = "fail";
                $returndata['message'] = 'User does not exist.' ;
            }
        }
        else {
            $returndata['status'] = "fail";
            $returndata['message'] = 'Please provide required fields.' ;
        }
        $this->apiResponse($returndata);
    }

    public function reset_password()
    {
    	$returndata = array();
    	$users = new Users();
    	$user_id = $this->request->getPost('user_id');
    	$password = $this->request->getPost('password');
    	if(!empty($user_id) && !empty($password) ){
    		$users->crud_update(array(
    			'password'=> $password
    		), $user_id);
			$returndata['status'] = "success";
			$returndata['message'] = "Password updated successfully.";
    	}
    	else {
    		$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
    	}
    	$this->apiResponse($returndata);
    }

    public function change_password()
    {
    	$returndata = array();
    	$users = new Users();
    	$user_id = $this->request->getPost('user_id');
    	$password = $this->request->getPost('password');
    	$old_password = $this->request->getPost('old_password');
    	if(!empty($user_id) && !empty($password) ){
			
			$user_data = $users->crud_read($user_id);
			foreach($user_data as $usersdata)
			{
				if($usersdata['password'] == $old_password)
				{
					$users->crud_update(array('password'=> $password
					), $user_id);
					
					$returndata['status'] = "success";
					$returndata['message'] = "Password updated successfully.";
				}else{
					$returndata['status'] = "fail";
					$returndata['message'] = "Old password does not match.";
				}
			}
    	}
    	else {
    		$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
    	}
    	$this->apiResponse($returndata);
    }
	
	public function get_user_profile()
	{
		$returndata = array();
		$users = new Users();
		$songs = new Songs();
		$follower_data = array();
		$following_data = array();
		$user_id = $this->request->getPost('user_id');
		if(!empty($user_id)){
			$user_data = $users->crud_read($user_id);
			$user_arr = array();
			foreach($user_data as $usersdata)
			{
				$usersdata['profile_img'] = base_url()."/writable/uploads/".$usersdata['profile_img'];
				
				$usersdata['total_follower'] = count($users->crud_read_follower($user_id));
				$usersdata['total_following'] = count($users->crud_read_following($user_id));
				$fav_count =0;
				$fav_list = $songs->curd_read_user_favorite($user_id);
				foreach($fav_list as $fav)
				{
					$user_videos = $songs->crud_read_video($fav['video_id']);
					if(count($user_videos) > 0)
					{
						$fav_count+= 1;
					}
				}
				$usersdata['total_favorite'] = $fav_count;
				/* echo $songs->getLastQuery();
				print_r($usersdata['total_favorite']);
				die(); */
				$user_arr[] = $usersdata;
			}
			//$returndata['follower'] = $follower_data;
			//$returndata['following'] = $following_data;
			
			$returndata['status'] = "success";
			$returndata['data'] = $user_arr;
			$returndata['message'] = "User fetched successfully.";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function get_fav_list()
	{
		$returndata = array();
		$songs = new Songs();
		$users = new Users();
		$block_unblock = new Block_unblock();
		$user_id = $this->request->getPost("user_id");
		if(!empty($user_id))
		{
			
			$fav_list = $songs->curd_read_user_favorite($user_id);
			//echo $songs->getLastQuery();
			/* echo "<pre>";
			print_r($fav_arr);
			die(); */
			$fav_arr = array();
			foreach($fav_list as $favs)
			{
				$user_videos = $songs->crud_read_video($favs['video_id']);
				foreach($user_videos as $video)
				{
				$user_fav_status = "";
				$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
				if($fav_status >0 )
				{
					$user_fav_status = 1;
				}else{
					$user_fav_status = 0;
				}
				
				$user_like_status = "";
				$like_status = $songs->crud_read_likes($user_id,$video['video_id']);
				if($like_status >0 )
				{
					$user_like_status = 1;
				}else{
					$user_like_status = 0;
				}
				
				$user_follow_status = "";
				$follow_status = $songs->crud_read_follow($user_id,$video['user_id']);
				if($follow_status >0 )
				{
					$user_follow_status = 1;
				}else{
					$user_follow_status = 0;
				}
				
				$block_list = $block_unblock->crud_read($user_id,$video['user_id']);
					if(!empty($block_list)){
						if($block_list[0]['block_status'] == 1){
							$user_block_status = 1;
						}else{
							$user_block_status = 0;
						}
					}else{
							$user_block_status = 0;
					}
					
				$block_list1 = $block_unblock->crud_read($video['user_id'], $user_id);
				if(!empty($block_list1)){
					if($block_list1[0]['block_status'] == 1){
						$friend_block_status = 1;
					}else{
						$friend_block_status = 0;
					}
				}else{
						$friend_block_status = 0;
				}
				
				if($user_block_status ==0 && $friend_block_status ==0)
				{
					$user_data = $users->crud_read($video['user_id']);
					$video['user_name'] = $user_data[0]['name'];
					$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
					$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
					$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
					$video['user_fav_status'] = $user_fav_status;
					$video['user_like_status'] = $user_like_status;
					$video['user_follow_status'] = $user_follow_status;
					$fav_arr[] = $video;
					
						/* $user_data = $users->crud_read($video['user_id']);
						$video['user_name'] = $user_data[0]['name'];
						$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
						$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
						$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
						$fav_arr[] = $video; */
					}else{
						continue;
					}
				}
					
			}
				
			$returndata['fav_list'] = $fav_arr;
			$returndata['status'] = "success";
			$returndata['message'] = "Favorite List fetched Successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_follower_profile()
	{
		$returndata = array();
		$users = new Users(); //||
		$songs = new Songs();
		$block_unblock = new Block_unblock();
		$user_id = $this->request->getPost('user_id');
		$friend_id = $this->request->getPost('friend_id');
		if((!empty($user_id)) || !empty($friend_id)){
			$userdata1 = array();
			$user_data = $users->crud_read($friend_id);
			foreach($user_data as $friend)
			{
				$userdata = array();
				
				$user_status = $songs->crud_read_follow($user_id,$friend_id);
				if($user_status > 0)
				{
					$user_follow_status = 1;
				}else{
					$user_follow_status = 0;
				}
				$friend_status = $songs->crud_read_follow($friend_id,$user_id);
				if($friend_status > 0)
				{
					$friend_follow_status = 1;
				}else{
					$friend_follow_status = 0;
				}
				
				$block_list = $block_unblock->crud_read($user_id,$friend_id);
					if(!empty($block_list)){
						if($block_list[0]['block_status'] == 1){
							$user_block_status = 1;
						}else{
							$user_block_status = 0;
						}
					}else{
							$user_block_status = 0;
					}
					
					$block_list1 = $block_unblock->crud_read($friend_id, $user_id);
					if(!empty($block_list1)){
						if($block_list1[0]['block_status'] == 1){
							$friend_block_status = 1;
						}else{
							$friend_block_status = 0;
						}
					}else{
							$friend_block_status = 0;
					}
				
				$userdata['user_block_status'] = $user_block_status;
				$userdata['friend_block_status'] = $friend_block_status;	
				$userdata['friend_id'] = $friend_id;
				$userdata['name'] = $friend['name'];
				$userdata['user_name'] = $friend['user_name'];
				$userdata['user_bio'] = $friend['bio'];
				$userdata['user_profile'] = base_url()."/writable/uploads/".$friend['profile_img'];
				
				$userdata['user_follow_status'] = $user_follow_status;
				$userdata['friend_follow_status'] = $friend_follow_status;
				$userdata['total_follower'] = count($users->crud_read_follower($friend_id));
				$userdata['total_following'] = count($users->crud_read_following($friend_id));
				$userdata1[] =  $userdata;
			}
			
			$returndata['status'] = "success";
			$returndata['data'] = $userdata;
			$returndata['message'] = "User Data fetched successfully.";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	/*   Get Follower & Following */
	public function get_follower_following()
	{
		$returndata = array();
		$users = new Users();
		$songs = new Songs();
		$block_unblock = new Block_unblock();
		$user_id = $this->request->getPost('user_id');
		$follower_data = array();
		$following_data = array();
		if(!empty($user_id)){
			$user_data = $users->crud_read($user_id);
			if(!empty($user_data))
			{
				$follower_res = $users->crud_read_follower($user_id);
				foreach($follower_res as $follower)
				{
					$user_status = $songs->crud_read_follow($follower['friend_id'],$follower['userid']);
					if($user_status > 0)
					{
						$user_follow_status = 1;
					}else{
						$user_follow_status = 0;
					}
					$friend_status = $songs->crud_read_follow($follower['userid'],$follower['friend_id']);
					if($friend_status > 0)
					{
						$friend_follow_status = 1;
					}else{
						$friend_follow_status = 0;
					}
					$block_list = $block_unblock->crud_read($follower['userid'], $follower['friend_id']);
					if(!empty($block_list)){
						if($block_list[0]['block_status'] == 1){
							$user_block_status = 1;
						}else{
							$user_block_status = 0;
						}
					}else{
							$user_block_status = 0;
					}
					
					$block_list1 = $block_unblock->crud_read($follower['friend_id'], $follower['userid']);
					if(!empty($block_list1)){
						if($block_list1[0]['block_status'] == 1){
							$friend_block_status = 1;
						}else{
							$friend_block_status = 0;
						}
					}else{
							$friend_block_status = 0;
					}
					
					$follower['user_block_status'] = $user_block_status;
					$follower['friend_block_status'] = $friend_block_status;
					$friend_data = $users->crud_read($follower['userid']);
					if(!empty($friend_data))
					{
					/* print_r($friend_data);
					die(); */
						$follower['name'] = $friend_data[0]['name'];
						$follower['user_name'] = $friend_data[0]['user_name'];
						$follower['user_profile'] = base_url()."/writable/uploads/".$friend_data[0]['profile_img'];
						$follower['user_follow_status'] = $user_follow_status;
						$follower['friend_follow_status'] = $friend_follow_status;
						$friendid = $follower['userid'];
						$useid = $follower['friend_id'];
						$follower['friend_id'] = $friendid;
						$follower['userid'] = $useid;
						$follower['total_follower'] = count($users->crud_read_follower($follower['friend_id']));
						
						$follower_data[] =  $follower;
					}
				}
				$following_res = $users->crud_read_following($user_id);
				foreach($following_res as $following)
				{
					$user_status = $songs->crud_read_follow($following['userid'],$following['friend_id']);
					if($user_status > 0)
					{
						$user_follow_status = 1;
					}else{
						$user_follow_status = 0;
					}
					$friend_status = $songs->crud_read_follow($following['friend_id'],$following['userid']);
					if($friend_status > 0)
					{
						$friend_follow_status = 1;
					}else{
						$friend_follow_status = 0;
					}
					
					$block_list = $block_unblock->crud_read($following['userid'], $following['friend_id']);
					if(!empty($block_list)){
						if($block_list[0]['block_status'] == 1){
							$user_block_status = 1;
						}else{
							$user_block_status = 0;
						}
					}else{
							$user_block_status = 0;
					}
					
					$block_list1 = $block_unblock->crud_read($following['friend_id'], $following['userid']);
					if(!empty($block_list1)){
						if($block_list1[0]['block_status'] == 1){
							$friend_block_status = 1;
						}else{
							$friend_block_status = 0;
						}
					}else{
							$friend_block_status = 0;
					}
					/* $friendid = $following['userid'];
					$useid = $following['friend_id'];
					$following['userid'] = $friendid;
					$following['friend_id'] = $useid; */
					
					$following['user_block_status'] = $user_block_status;
					$following['friend_block_status'] = $friend_block_status;
					
					$friend_data = $users->crud_read($following['friend_id']);
					
					if(!empty($friend_data))
					{
						$following['name'] = $friend_data[0]['name'];
						$following['user_name'] = $friend_data[0]['user_name'];
						$following['user_profile'] = base_url()."/writable/uploads/".$friend_data[0]['profile_img'];
						
						$following['user_follow_status'] = $user_follow_status;
						$following['friend_follow_status'] = $friend_follow_status;
						$following['total_follower'] = count($users->crud_read_follower($following['friend_id']));
						$following_data[] =  $following;
					}
				}
			}
			$returndata['follower'] = $follower_data;
			$returndata['following'] = $following_data;
			$returndata['status'] = "success";
			$returndata['message'] = "User data fetched successfully.";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
		/*   Get Friend Follower & Following */
	public function get_friend_follower_following()
	{
		$returndata = array();
		$users = new Users();
		$songs = new Songs();
		$block_unblock = new Block_unblock();
		$user_id = $this->request->getPost('user_id');
		$friend_id = $this->request->getPost('friend_id');
		$follower_data = array();
		$following_data = array();
		if(!empty($user_id)){
			$user_data = $users->crud_read($friend_id);
			if(!empty($user_data))
			{
				$follower_res = $users->crud_read_follower($friend_id);
				foreach($follower_res as $follower)
				{
					$user_status = $songs->crud_read_follow($user_id,$follower['userid']);
					if($user_status > 0)
					{
						$user_follow_status = 1;
					}else{
						$user_follow_status = 0;
					}
					$friend_status = $songs->crud_read_follow($follower['userid'],$user_id);
					if($friend_status > 0)
					{
						$friend_follow_status = 1;
					}else{
						$friend_follow_status = 0;
					}
					$block_list = $block_unblock->crud_read($user_id, $follower['userid']);
					if(!empty($block_list)){
						if($block_list[0]['block_status'] == 1){
							$user_block_status = 1;
						}else{
							$user_block_status = 0;
						}
					}else{
							$user_block_status = 0;
					}
					
					$block_list1 = $block_unblock->crud_read($follower['userid'], $user_id);
					if(!empty($block_list1)){
						if($block_list1[0]['block_status'] == 1){
							$friend_block_status = 1;
						}else{
							$friend_block_status = 0;
						}
					}else{
							$friend_block_status = 0;
					}
					
					$follower['user_block_status'] = $user_block_status;
					$follower['friend_block_status'] = $friend_block_status;
					$friend_data = $users->crud_read($follower['userid']);
					$follower['user_name'] = $friend_data[0]['name'];
					$follower['user_profile'] = base_url()."/writable/uploads/".$friend_data[0]['profile_img'];
					$follower['user_follow_status'] = $user_follow_status;
					$follower['friend_follow_status'] = $friend_follow_status;
					$friendid = $follower['userid'];
					$useid = $follower['friend_id'];
					$follower['friend_id'] = $friendid;
					$follower['userid'] = $useid;
					$follower['total_follower'] = count($users->crud_read_follower($follower['friend_id']));
					
					$follower_data[] =  $follower;
					
				}
				$following_res = $users->crud_read_following($friend_id);
				foreach($following_res as $following)
				{
					$user_status = $songs->crud_read_follow($user_id,$following['friend_id']);
					if($user_status > 0)
					{
						$user_follow_status = 1;
					}else{
						$user_follow_status = 0;
					}
					$friend_status = $songs->crud_read_follow($following['friend_id'],$user_id);
					if($friend_status > 0)
					{
						$friend_follow_status = 1;
					}else{
						$friend_follow_status = 0;
					}
					
					$block_list = $block_unblock->crud_read($user_id, $following['friend_id']);
					if(!empty($block_list)){
						if($block_list[0]['block_status'] == 1){
							$user_block_status = 1;
						}else{
							$user_block_status = 0;
						}
					}else{
							$user_block_status = 0;
					}
					
					$block_list1 = $block_unblock->crud_read($following['friend_id'], $user_id);
					if(!empty($block_list1)){
						if($block_list1[0]['block_status'] == 1){
							$friend_block_status = 1;
						}else{
							$friend_block_status = 0;
						}
					}else{
							$friend_block_status = 0;
					}
					/* $friendid = $following['userid'];
					$useid = $following['friend_id'];
					$following['userid'] = $friendid;
					$following['friend_id'] = $useid; */
					
					$following['user_block_status'] = $user_block_status;
					$following['friend_block_status'] = $friend_block_status;
					
					$friend_data = $users->crud_read($following['friend_id']);
					$following['user_name'] = $friend_data[0]['name'];
					$following['user_profile'] = base_url()."/writable/uploads/".$friend_data[0]['profile_img'];
					
					$following['user_follow_status'] = $user_follow_status;
					$following['friend_follow_status'] = $friend_follow_status;
					$following['total_follower'] = count($users->crud_read_follower($following['friend_id']));
					$following_data[] =  $following;
				}
			}
			$returndata['follower'] = $follower_data;
			$returndata['following'] = $following_data;
			$returndata['status'] = "success";
			$returndata['message'] = "User data fetched successfully.";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}

		/*-------------Privacy Policy & T&C API---------------*/

 	public function get_content()
	{
		$returndata = array();
		$content = new Content();
		$content_data = $content->crud_read();
		$new_arr = array();
		foreach ($content_data as $data) {
			if($data['content_id'] =="1")
			{
				$new_arr['Privacy_policies']= array(
					"page_name" => $data['page_name'],
					"page_content" => preg_replace("/\s+/", " ", $data['page_content'])
				);
			} 
			else if($data['content_id'] =="2")
			{
				$new_arr['Terms_and_conditions']= array(
					"page_name" => $data['page_name'],
					"page_content" => preg_replace("/\s+/", " ", $data['page_content'])
				);
			}
		}
		$returndata['content_data'] = $new_arr;
		$returndata['status'] = "success";
		$returndata['message'] = "Content list";
		return $this->respond($returndata,200);
	}

	public function update_user_profile()
	{
		$returndata = array();
		$users = new Users();
		$user_id = $this->request->getPost('user_id');
		$name = $this->request->getPost("name");
		$user_name = $this->request->getPost("user_name");
		$mobile = $this->request->getPost("mobile");
		$email = $this->request->getPost("email");
		//$password = $this->request->getPost("password");
		$bio = $this->request->getPost("bio");
		$user_image = $this->request->getFile("user_image");
		$user_data = $users->crud_read($user_id);
		if(!empty($user_id)){
			if($user_data){
					if($user_image->isValid()){
						$photopath = $user_image->store();
						if($photopath != ""){
							//echo $photopath;
							//die();
							$users->crud_update(array(
								'user_name' => $user_name,
								'name' => $name,
								'user_email' => $email,
								'mobile' => $mobile,
								'bio' => $bio,
								'profile_img' => $photopath 
							),$user_id);
						}else{
							$users->crud_update(array(
							'user_name' => $user_name,
							'name' => $name,
							'user_email' => $email,
							'mobile' => $mobile,
							'bio' => $bio
						), $user_id);
						}
					}
				$returndata['status'] = "success";
				$returndata['message'] = "User updated successfully.";
			}
			else {
				$returndata['status'] = "success";
				$returndata['message'] = "User does not exist.";
			}
		}
		else {
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}

	public function block_unblock()
	{
		$returndata = array();
		$block_unblock = new Block_unblock();
		$user_id = $this->request->getPost('user_id');
		$followerid = $this->request->getPost('followerid');
		$block_list = $block_unblock->crud_read($user_id, $followerid);
		if(!empty($block_list)){
			if($block_list[0]['block_status'] == 1){
				$block_unblock->crud_update(array(
					'userid' => $user_id,
					'followerid' => $followerid,
					'block_status' => 0,
					'date_added' => date('Y-m-d H:i:s')
				), $block_list[0]['block_id']);
				$returndata['status'] = "success";
				$returndata['message'] = "User unblocked successfully.";
			}
			else {
				$block_unblock->crud_update(array(
					'userid' => $user_id,
					'followerid' => $followerid,
					'block_status' => 1,
					'date_added' => date('Y-m-d H:i:s')
				), $block_list[0]['block_id']);
				$returndata['status'] = "success";
				$returndata['message'] = "User blocked successfully.";
			}
		}
		else {
			$block_unblock->crud_create(array(
				'userid' => $user_id,
				'followerid' => $followerid,
				'block_status' => 1,
				'date_added' => date('Y-m-d H:i:s')
			));
			$returndata['status'] = "success";
			$returndata['message'] = "User blocked successfully.";
		}
		$this->apiResponse($returndata);
	}

	public function get_block_list()
	{
		$returndata = array();
		$block_unblock = new Block_unblock();
		$users = new Users();
		$user_id = $this->request->getPost('user_id');
		if(!empty($user_id)){
			$block_arr = array();
			$block_list = $block_unblock->crud_read($user_id);
			if(!empty($block_list))
			{
				foreach($block_list as $block_user)
				{
					
					$userdata = $users->crud_read($block_user['followerid']);
					$block_user['name'] = $userdata[0]['name'];
					$block_user['user_profile'] = base_url()."/writable/uploads/".$userdata[0]['profile_img'];
					$block_user['total_follower'] = count($users->crud_read_follower($userdata['followerid']));
					$block_arr[] =  $block_user;
				}
			}		
			$returndata['status'] = "success";
			$returndata['data'] = $block_arr;
        	$returndata['message'] = "Block list fetched successfully.";
		}
		else {
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	} 
	
	public function user_subscription_plan()
	{
		$returndata = array();
		$users = new users();
		$subscription = new Subscription();
		$user_id = $this->request->getPost('user_id');
		if(!empty($user_id)){
			$plan_history =array();
			$user_data = $users->crud_read($user_id);
			$plan_history = $subscription->crud_read_user_plan($user_id);
			
			//echo "<pre>";
			//print_r($plan_history);
			//die();
			//$plan_deatails = $subscription->crud_read($user_data[0]['planid']);
			$returndata['myplan_details'] = $plan_history;
			$subscription_data = $subscription->crud_read();
			$subscription_details = array();
			foreach ($subscription_data as $data) {
				$subscription_details[] = array(
					"plan_id" => $data['plan_id'],
					"plan_name" => $data['plan_name'],
					"plan_amount" => $data['plan_amount'],
					"plan_swipes" => $data['plan_swipes'],
					"plan_status" => $data['plan_status'],
					"apple_product_id" => $data['apple_product_id'],
					"plan_details" => $data['plan_details'],
					"plan_image" => base_url()."/writable/uploads/".$data['plan_image']
					//"plan_desc" => $new_arr
				);
			}
			
			$returndata['current_date'] = date('Y-m-d H:i:s');
			$returndata['subscription_plan'] = $subscription_details;
			$returndata['status'] = "success";
			$returndata['message'] = "Subscription Plan fetched successfully.";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
		/* -------------set subscription plan --------- */

	public function set_subscription_plan()
	{
		$returndata = array();
		$users = new Users();
		$user_id = $this->request->getPost('user_id');
		$plan_id = $this->request->getPost("planid");
		$tx_id = $this->request->getPost('tx_id');
		$expire_on = $this->request->getPost('expire_on');
		$check = $this->request->getPost('check');
		$createdat = date('Y-m-d H:i:s');
		$subscription = new Subscription();
		$user_data = $users->crud_read($user_id);
		
		if(!empty($user_data)){
			
			if($check == "1"){
				$plan_data = $subscription->crud_read($plan_id);
				if($plan_data){
					/* $datetime = date('m/d/Y H:i');
					$game_datetime = $games['game_date']." ".$games['game_time'];
					
					if(strtotime($datetime) >= strtotime($game_datetime)){
					}
					$plan_history = $subscription->crud_read_user_plan($user_id);
					$subscription->update_user_tx(array("status" => 2),$user_id); */
					
					$subscription->create_user_tx(array(
						"transaction_token" => $tx_id,
						"userid" => $user_id,
						"planid" => $plan_data[0]['plan_id'],
						"amount" => $plan_data[0]['plan_amount'],
						"createdat" => $createdat,
						"expire_on" => $expire_on
						//"status" => 1
					));
				}
			}				
			
			$returndata['status'] = "success";
			$returndata['current_date'] = date('Y-m-d H:i:s');
			$returndata['message'] = "User subsription Plan saved successfully ";
		} else {
			$returndata['status'] = "error";
			$returndata['message'] = "User doesn't exist";
		}
		return $this->respond($returndata,200);
	}

	public function logout_delete()
	{
		$returndata = array();
		$type = $this->request->getPost("type");
		$user_id = $this->request->getPost("user_id");
		$users = new Users();
		$userdata = $users->crud_read($user_id);
		if(count($userdata) < 1){
			$returndata['status'] = "error";
			$returndata['message'] = "Invalid User!";
			return $this->respond($returndata,200);
		}
		$userdata = $userdata[0];
		if($type == 1){
			$users->crud_update(array("is_active" => 0),$user_id);
			$returndata['status'] = "success";
			$returndata['message'] = "Logged out successfully";
		}
		elseif($type == 2){
			$users->crud_delete($user_id);
			$returndata['status'] = "success";
			$returndata['message'] = "Account Deleted successfully";
		}
		else{
			$returndata['status'] = "fail";
			$returndata['message'] = "Invalid Type!";
		}
		$this->apiResponse($returndata);
	}
	
	/* -----------------new API ------- */
	
	public function check_username_is_valid()
	{
		$returndata = array();
		$user_name = $this->request->getPost("username");
		$users = new Users();
		$res_data = $users->check_username_exists($user_name);
		if($res_data > 0)
		{
			$username = substr(str_replace(' ','',strtolower($user_name)), 0, 10).rand(1,99);
			
			$userdata = $users->check_username_exists($username);
			if($userdata > 0)
			{
				$username = substr(str_replace(' ','',strtolower($username)), 0, 10).rand(1,99);
			}else{
				$suggested_username = $username;
			}
			$returndata['status'] = "fail";
			$returndata['message'] = "Username exists!";
			$returndata['suggested'] = $suggested_username;
			return $this->respond($returndata,200);
		}else{
			
			$returndata['status'] = "success";
			$returndata['message'] = "Username not exists!";
			return $this->respond($returndata,200);
		}
		$this->apiResponse($returndata);
	}
	
	
	public function social_login()
	{
		$returndata = array();
		$users = new Users();
		$name = $this->request->getPost("name");
		$email = $this->request->getPost("email");
		$photo = $this->request->getFile("photo");
		$social_type = $this->request->getPost('social_type');
		
		if((!empty($name) || !empty($email)) && !empty($social_type))
		{
			$check_email = $users->check_email_exists($email);
			if($check_email > 0)
			{
				$user_id = $check_email;
			}else{
				if($photo){
					if($photo->isValid()){
						$photopath = $photo->store();
						if($photopath != ""){
							$user_id = $users->crud_create(array(
								'name' => $name,
								'user_email' => $email,
								'social_type' => $social_type,
								'is_active' => 1,
								'profile_img' => $photopath 
							));
						}
					}
				}else{
							$user_id = $users->crud_create(array(
								'name' => $name,
								'user_email' => $email,
								'is_active' => 1,
								'social_type' => $social_type
							));
				}
			}
			$returndata['user_id'] = $user_id;
			$returndata['status'] = "success";
        	$returndata['message'] = "User registered successfully";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
		/*-------------Reporte User API---------------*/

	public function report_user()
	{
		$returndata = array();
		$users = new Users();
		$user_id = $this->request->getPost('user_id');
		$reported_userid = $this->request->getPost('reported_userid');
		$report_msg = $this->request->getPost('report_msg');
		$date = date('Y-m-d H:i:s');
		$users->report_user(
			array(
				"userid" => $user_id,
				"reported_userid" => $reported_userid,
				"report_msg" => $report_msg,
				"date_added" => $date
			)
		);
		/* $reported_count = count($users->read_report_user($reported_user_id,$user_id));
		if($reported_count >= 3 )
		{
			$users->crud_update(array(
				"reported_count" => $reported_count,
				"is_active" => 0
				),$reported_user_id);
		} else {
			$users->crud_update(array("reported_count" => $reported_count),$reported_user_id);
		} */
		$returndata['status'] = "success";
		$returndata['message'] = "User reported";
		return $this->respond($returndata,200);
	}
	
		/*-------------Get Home Data API---------------*/
	
	public function get_home_data()
	{
		$returndata = array();
		$data =array();
		$users = new Users();
		$songs = new Songs();
		$notifications = new Notifications();
		$block_unblock = new Block_unblock();
		$user_id = $this->request->getPost("user_id");
		$data['latitude'] = $this->request->getPost("lat");
		$data['longitude'] = $this->request->getPost("long");
		$data['min_range'] = $this->request->getPost("min_range");
		$data['max_range'] = $this->request->getPost("max_range");
		$fcmtoken = $this->request->getPost("fcm_token");
		$userdata = $users->crud_read($user_id);
		if(!empty($user_id)){
			//$user_data = $users->read_home_data($data);
			$users->crud_update(array('fcmtoken' => $fcmtoken),$user_id);
			$video_arr = array();
			$notification_list = $notifications->crud_read_notification($user_id);
			if(count($notification_list) > 0)
			{
				// // for old user , display video according to his activities like , comment , follow , 
				foreach($notification_list as $notification)
				{
					$user_data = $users->crud_read($notification['friendid']);
					$notification['name'] = $user_data[0]['name'];
					$notification['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
						
					$user_videos = $songs->crud_read_user_video($notification['friendid']);
					foreach($user_videos as $video)
					{
						$user_fav_status = "";
						$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
						if($fav_status >0 )
						{
							$user_fav_status = 1;
						}else{
							$user_fav_status = 0;
						}
						
						$user_like_status = "";
						$like_status = $songs->crud_read_likes($user_id,$video['video_id']);
						if($like_status >0 )
						{
							$user_like_status = 1;
						}else{
							$user_like_status = 0;
						}
						
						$user_follow_status = "";
						$follow_status = $songs->crud_read_follow($user_id,$video['user_id']);
						if($follow_status >0 )
						{
							$user_follow_status = 1;
						}else{
							$user_follow_status = 0;
						}
						
						$block_list = $block_unblock->crud_read($user_id,$video['user_id']);
							if(!empty($block_list)){
								if($block_list[0]['block_status'] == 1){
									$user_block_status = 1;
								}else{
									$user_block_status = 0;
								}
							}else{
									$user_block_status = 0;
							}
							
						$block_list1 = $block_unblock->crud_read($video['user_id'], $user_id);
						if(!empty($block_list1)){
							if($block_list1[0]['block_status'] == 1){
								$friend_block_status = 1;
							}else{
								$friend_block_status = 0;
							}
						}else{
								$friend_block_status = 0;
						}
						
						if($user_block_status ==0 && $friend_block_status ==0)
						{
							if(!empty($video['old_video_id']))
							{
								$video_data = $songs->crud_read_video($video['old_video_id']);
								
								if(!empty($video_data))
								{
									$video['main_video_file'] = base_url()."/writable/uploads/".$video_data[0]['video_file'];
								}else{
									$video['main_video_file'] = base_url()."/writable/uploads/".$video['video_file'];
								}
							}else{
								$video['main_video_file'] = base_url()."/writable/uploads/".$video['video_file'];
							}
							
							$video['user_block_status'] = $user_block_status;
							$video['friend_block_status'] = $friend_block_status;
							$user_data = $users->crud_read($video['user_id']);
							$video['user_name'] = $user_data[0]['name'];
							$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
							$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
							$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
							$video['user_fav_status'] = $user_fav_status;
							$video['user_like_status'] = $user_like_status;
							$video['user_follow_status'] = $user_follow_status;
							$video_arr[] = $video;
						}else{
							continue;
						}
					}
				}
			}else{
				//echo "new user";
				// for new user , display video according to his music choice
				$choice_list = $songs->curd_read_music_choice($user_id);
				
				if(count($choice_list)>0)
				{
					foreach($choice_list as $choice)
					{
						$user_videos = $songs->crud_read_video_by_cat($choice['music_cat_id']);
						/* echo $songs->getLastQuery();
						print_r($user_videos);
						die(); */
						foreach($user_videos as $video)
						{
							$user_fav_status = "";
							$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
							if($fav_status >0 )
							{
								$user_fav_status = 1;
							}else{
								$user_fav_status = 0;
							}
							
							$user_like_status = "";
							$like_status = $songs->crud_read_likes($user_id,$video['video_id']);
							if($like_status >0 )
							{
								$user_like_status = 1;
							}else{
								$user_like_status = 0;
							}
							
							$user_follow_status = "";
							$follow_status = $songs->crud_read_follow($user_id,$video['user_id']);
							if($follow_status >0 )
							{
								$user_follow_status = 1;
							}else{
								$user_follow_status = 0;
							}
							
							$block_list = $block_unblock->crud_read($user_id,$video['user_id']);
								if(!empty($block_list)){
									if($block_list[0]['block_status'] == 1){
										$user_block_status = 1;
									}else{
										$user_block_status = 0;
									}
								}else{
										$user_block_status = 0;
								}
								
							$block_list1 = $block_unblock->crud_read($video['user_id'], $user_id);
							if(!empty($block_list1)){
								if($block_list1[0]['block_status'] == 1){
									$friend_block_status = 1;
								}else{
									$friend_block_status = 0;
								}
							}else{
									$friend_block_status = 0;
							}
							
							if($user_block_status ==0 && $friend_block_status ==0)
							{
								if(!empty($video['old_video_id']))
								{
									$video_data = $songs->crud_read_video($video['old_video_id']);
									
									if(!empty($video_data))
									{
										$video['main_video_file'] = base_url()."/writable/uploads/".$video_data[0]['video_file'];
									}else{
										$video['main_video_file'] = base_url()."/writable/uploads/".$video['video_file'];
									}
								}else{
									$video['main_video_file'] = base_url()."/writable/uploads/".$video['video_file'];
								}
								
								$video['user_block_status'] = $user_block_status;
								$video['friend_block_status'] = $friend_block_status;
								$user_data = $users->crud_read($video['user_id']);
								$video['user_name'] = $user_data[0]['name'];
								$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
								$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
								$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
								$video['user_fav_status'] = $user_fav_status;
								$video['user_like_status'] = $user_like_status;
								$video['user_follow_status'] = $user_follow_status;
								$video_arr[] = $video;
							}else{
								continue;
							}
						}
					}
				}
			}
			$song_list = $songs->crud_read();
			if(!empty($video_arr))
			{
				$returndata['video_list'] = $video_arr;
				$returndata['latest_song_list'] = $song_list;
				$returndata['status'] = "success";
				$returndata['message'] = "home data";
			} else {
				$returndata['status'] = "success";
				$returndata['message'] = "No data found";
			}
		}
		else{
			$returndata['status'] = "error";
			$returndata['message'] = "Please enter valid parameters";
		}
		return $this->respond($returndata,200);
	}
	
	public function upload_video()
	{
		$returndata = array();
		$songs = new Songs();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$music_cat_id = $this->request->getPost("music_cat_id");
		$video = $this->request->getFile("video");
		$lyrics = $this->request->getPost("lyrics");
		$artist_name = $this->request->getPost("artist_name");
		$song_name = $this->request->getPost("song_name");
		$video_thumb = $this->request->getFile('video_thumb');
		$old_video_id = $this->request->getPost("old_video_id");
		if(!empty($user_id) || !empty($video))
		{
			$photopath = "";
			$photopath1 = "";
			if($video){
				if($video->isValid()){
					$photopath = $video->store();
				}
			}
			
			if($video_thumb){
				if($video_thumb->isValid()){
					$photopath1 = $video_thumb->store();
				}
			}
				
				
			$song_id = $songs->upload_video(array(
							'user_id' => $user_id,
							'video_file' => $photopath,
							'music_cat_id' => $music_cat_id,
							'artist_name' => $artist_name,
							'song_name' => $song_name,
							'video_thumb' => $photopath1,
							'created_on' => $date
						));
						
						if(!empty($old_video_id))
						{
							$user_videos = $songs->crud_read_video($old_video_id);
							$total_sing = $user_videos[0]['total_sing'] + 1;
							$songs->crud_update_video(array(
								'total_sing' => $total_sing
								),$old_video_id);
							
							$songs->crud_update_video(array('old_video_id' => $old_video_id,'main_video_id' => $user_videos[0]['main_video_id']
								),$song_id);
								
						}else{
							$songs->crud_update_video(array('old_video_id' => $song_id,'main_video_id' => $song_id
								),$song_id);
						}
						
						
			
			$returndata['status'] = "success";
        	$returndata['message'] = "Video uploaded successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function report_post()
	{
		$returndata = array();
		$songs = new Songs();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$video_id = $this->request->getPost("video_id");
		$comment = $this->request->getPost("comment");
		if((!empty($user_id) || !empty($video_id)) && !empty($comment))
		{
			$check_old_record = $songs->crud_read_post_report($user_id,$video_id);
			if($check_old_record > 0)
			{
				
			}else{
				$song_id = $songs->crud_create_post_report(array(
					'userid' => $user_id,
					'videoid' => $video_id,
					'comment' => $comment,
					'created_on' => $date
				));
				
				$video_data = $songs->crud_read_video($video_id);
				if(!empty($video_data))
				{
					$total_reports = $video_data[0]['total_reported'] + 1;
					$songs->crud_update_video(array('total_reported' => $total_reports
					),$video_id);
				}
			
			}
			$returndata['status'] = "success";
        	$returndata['message'] = "Post Reported successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function post_comment()
	{
		$returndata = array();
		$songs = new Songs();
		$users = new Users();
		$notifications = new Notifications();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$video_id = $this->request->getPost("video_id");
		$comment = $this->request->getPost("comment");
		if((!empty($user_id) || !empty($video_id)) && !empty($comment))
		{
			$song_id = $songs->crud_create_post_comment(array(
				'user_id' => $user_id,
				'video_id' => $video_id,
				'comment' => $comment,
				'is_visible' => 0,
				'created_on' => $date
			));
			$video_data = $songs->crud_read_video($video_id);
			if(!empty($video_data))
			{
				$total_comments = $video_data[0]['total_comments'] + 1;
				$songs->crud_update_video(array('total_comments' => $total_comments
				),$video_id);
				$userdata = $users->crud_read($user_id);
				$userdata1 = $users->crud_read($video_data[0]['user_id']);
				if(!empty($userdata1[0]['fcmtoken']))
				{
					$token = $userdata1[0]['fcmtoken'];	
					$title = "Comment Alert";
					$message = $userdata[0]['name']. " commented on your post "."\"" .$comment."\"";
					
					//$message = $userdata[0]['name']. " commented on your post, ".$comment;
					$this->sendGCM($token,$message,$title);
				}
				$notifications->crud_create(array(
				'friendid' => $user_id,
				'userid' => $video_data[0]['user_id'],
				'message' => $message,
				'type' => "Comment",
				'videoid' => $video_id,
				'notification_status' => 1,
				'created_at' => $date

				));
			}
			
			
			$returndata['status'] = "success";
        	$returndata['message'] = "Comment added to post successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function post_comment_reply()
	{
		$returndata = array();
		$songs = new Songs();
		$users = new Users();
		$notifications = new Notifications();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$comment_id = $this->request->getPost("comment_id");
		$comment = $this->request->getPost("comment");
		$video_id = $this->request->getPost("video_id");
		if((!empty($user_id) || !empty($comment_id)) && !empty($comment) && !empty($video_id))
		{
			
			$song_id = $songs->crud_create_post_comment_reply(array(
				'user_id' => $user_id,
				'comment_id' => $comment_id,
				'comment' => $comment,
				'is_visible' => 0,
				'created_on' => $date,
				'total_likes' => 0
			));
			$video_data = $songs->crud_read_video($video_id);
			if(!empty($video_data))
			{
				$total_comments = $video_data[0]['total_comments'] + 1;
				$songs->crud_update_video(array('total_comments' => $total_comments
				),$video_id);
				
				$userdata = $users->crud_read($user_id);
				$userdata1 = $users->crud_read($video_data[0]['user_id']);
				if(!empty($userdata1[0]['fcmtoken']))
				{
					$token = $userdata1[0]['fcmtoken'];	
					$title = "Comment Alert";
					$message = $userdata[0]['name']. " commented on your post "."\"" .$comment."\"";
					
					//$message = $userdata[0]['name']. " commented on your post, ".$comment;
					$this->sendGCM($token,$message,$title);
				}
				$notifications->crud_create(array(
				'friendid' => $user_id,
				'userid' => $video_data[0]['user_id'],
				'message' => $message,
				'type' => "Comment",
				'videoid' => $video_id,
				'notification_status' => 1,
				'created_at' => $date

				));
			}
			
			$returndata['status'] = "success";
        	$returndata['message'] = "Reply on comment added to post successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function comment_likes_unlike()
	{
		$returndata = array();
		$songs = new Songs();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$comment_id = $this->request->getPost("comment_id");
		$like_status = $this->request->getPost("like_status");
		if(!empty($user_id) && !empty($comment_id))
		{
			$check_previous_record = $songs->crud_read_comment_likes_unlikes($user_id,$comment_id);
			if($check_previous_record > 0)
			{
				$song_id = $songs->crud_update_comment_likes_unlikes(array(
					'like_status' => $like_status,
					'created_on' => $date
				),$check_previous_record);
				
			}else{
				
				$song_id = $songs->crud_create_comment_likes_unlikes(array(
					'user_id' => $user_id,
					'comment_id' => $comment_id,
					'like_status' => $like_status,
					'created_on' => $date
				));
			}
			
			//echo $songs->getLastQuery();
			//die();
			$comment_data = $songs->curd_read_comment($comment_id);
			if(!empty($comment_data))
			{
				if($like_status == "1")
				{
					$total_likes = $comment_data[0]['total_likes'] + 1;
					$msg = "You have liked this comment!";
				}else{
					$total_likes = $comment_data[0]['total_likes'] - 1;
					$msg = "You have unliked this comment!";
				}
				
				$songs->crud_update_comment(array('total_likes' => $total_likes
				),$comment_id);
			}
			
			
			$returndata['status'] = "success";
			$returndata['message'] = $msg;
        	
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function comment_reply_likes_unlike()
	{
		$returndata = array();
		$songs = new Songs();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$reply_id = $this->request->getPost("reply_id");
		$like_status = $this->request->getPost("like_status");
		if(!empty($user_id) && !empty($reply_id))
		{
			$check_previous_record = $songs->crud_read_reply_likes_unlikes($user_id,$reply_id);
			if($check_previous_record > 0)
			{
				$song_id = $songs->crud_update_reply_likes_unlikes(array(
					'like_status' => $like_status,
					'created_on' => $date
				),$check_previous_record);
				
			}else{
				
				$song_id = $songs->crud_create_reply_likes_unlikes(array(
					'user_id' => $user_id,
					'reply_id' => $reply_id,
					'like_status' => $like_status,
					'created_on' => $date
				));
			}
			
			//echo $songs->getLastQuery();
			//die();
			$reply_data = $songs->curd_read_reply($reply_id);
			if(!empty($reply_data))
			{
				if($like_status == "1")
				{
					$total_likes = $reply_data[0]['total_likes'] + 1;
					$msg = "You have liked this comment!";
				}else{
					$total_likes = $reply_data[0]['total_likes'] - 1;
					$msg = "You have unliked this comment!";
				}
				
				$songs->crud_update_comment_reply(array('total_likes' => $total_likes
				),$reply_id);
			}
			
			
			$returndata['status'] = "success";
			$returndata['message'] = $msg;
        	
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function post_likes_unlike()
	{
		$returndata = array();
		$songs = new Songs();
		$users = new Users();
		$notifications = new Notifications();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$video_id = $this->request->getPost("video_id");
		$like_status = $this->request->getPost("like_status");
		if(!empty($user_id) && !empty($video_id))
		{
			$check_previous_record = $songs->crud_read_post_likes_unlikes($user_id,$video_id);
			if($check_previous_record > 0)
			{
				$song_id = $songs->crud_update_post_likes_unlikes(array(
					'like_status' => $like_status,
					'created_on' => $date
				),$check_previous_record);
				
			}else{
				$song_id = $songs->crud_create_post_likes_unlikes(array(
					'user_id' => $user_id,
					'video_id' => $video_id,
					'like_status' => $like_status,
					'created_on' => $date
				));
			}
			
			//echo $songs->getLastQuery();
			//die();
			
			$video_data = $songs->crud_read_video($video_id);
			if(!empty($video_data))
			{
				$userdata = $users->crud_read($user_id);
				$userdata1 = $users->crud_read($video_data[0]['user_id']);
				if($like_status == "1")
				{
					$total_likes = $video_data[0]['total_likes'] + 1;
					$msg = "You have liked this post!";
					if(empty($userdata1[0]['fcmtoken']))
					{
						$token = "fsOu5DdsJ0LRvZ6PMuY0rF:APA91bFKhmEyWoaqIK3KIK63TlNNHTQ8I-QTkNlR011YjEttsjwMIjhwrTcojh9JQwEvHNj7Tlp0occPzHj1BA4zqv3JV0_P3vHCYcoK3DbgOhn2PXSCCvtncfpgQ5KH3KCneCN--3H5";

					}else{
						$token = $userdata1[0]['fcmtoken'];
					}
					
					$title = "Like Alert";
					$message = $userdata[0]['name']. " liked your post.";
					$this->sendGCM($token,$message,$title);
					
					$notifications->crud_create(array(
					'friendid' => $user_id,
					'userid' => $video_data[0]['user_id'],
					'message' => $message,
					'type' => "Like",
					'videoid' => $video_id,
					'notification_status' => 1,
					'created_at' => $date

					));
				}else{
					$total_likes = $video_data[0]['total_likes'] - 1;
					$msg = "You have unliked this post!";
				}
				
				$songs->crud_update_video(array('total_likes' => $total_likes
				),$video_id);
			}
			
			
			$returndata['status'] = "success";
			$returndata['message'] = $msg;
        	
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_music_category()
	{
		$returndata = array();
		$songs = new Songs();
		$music_cat = array();
		$music_data = $songs->curd_read_music_category();	
		foreach($music_data as $music)
		{
			$music['category_img'] = base_url()."/writable/uploads/".$music['category_img'];
			$music_cat[] = $music;
		}
		$returndata['music_category'] = $music_cat;
		$returndata['status'] = "success";
		$returndata['message'] = "Music Category fetched Successfully!";
		$this->apiResponse($returndata);
	}
	
	public function get_comment_list()
	{
		$returndata = array();
		$songs = new Songs();
		$users = new Users();
		$user_id = $this->request->getPost("user_id");
		$video_id = $this->request->getPost("video_id");
		if((!empty($user_id)) && (!empty($video_id)))
		{	
			$user_comment_arr =array();
			$all_comment_arr =array();
			$user_comment = $songs->curd_read_user_comment($user_id,$video_id);
			
			if(!empty($user_comment))
			{
				foreach($user_comment as $comment)
				{
					$user_data = $users->crud_read($comment['user_id']);
					$comment['user_name'] = $user_data[0]['name'];
					if(!empty($user_data[0]['profile_img']))
					{
						$comment['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
					}else{
						$comment['user_profile'] = "";
					}
					
					
					$comment_like = $songs->crud_read_comment_likes_unlikes($user_id,$comment['comments_id']);
					if($comment_like > 0)
					{
						$like_status = 1;
					}else{
						$like_status = 0;
					}
					$comment['comments_like_status'] = $like_status;
					
					 $reply_data = $songs->curd_read_comment_reply($comment['comments_id']);
					 if(!empty($reply_data))
					 {
						 foreach($reply_data as $reply)
						 {
							$reply_like = $songs->crud_read_reply_likes_unlikes($user_id,$reply['reply_id']);
							if($reply_like > 0)
							{
								$reply_status = 1;
							}else{
								$reply_status = 0;
							}
							$reply['reply_like_status'] = $reply_status;
					
							$user_data = $users->crud_read($reply['user_id']);
							$reply['user_name'] = $user_data[0]['name'];
							if(!empty($user_data[0]['profile_img']))
							{
								$reply['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
							}else{
								$reply['user_profile'] = "";
							}
							
							$comment['reply_on_comment'][] = $reply;
						 }
						
					 }else{
						 $comment['reply_on_comment'] = array();
					 }
					 
					
					$user_comment_arr[] = $comment;
				}
			}
			//echo $songs->getLastQuery();
					/* echo "<pre>";
					print_r($user_comment_arr);
					die(); */
			$all_comment = $songs->curd_read_all_comment($video_id);
			if(!empty($all_comment))
			{
				foreach($all_comment as $comment)
				{
					$user_data = $users->crud_read($comment['user_id']);
					$comment['user_name'] = $user_data[0]['name'];
					if(!empty($user_data[0]['profile_img']))
					{
						$comment['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
					}else{
						$comment['user_profile'] = "";
					}
					
					$comment_like = $songs->crud_read_comment_likes_unlikes($user_id,$comment['comments_id']);
					if($comment_like > 0)
					{
						$like_status = 1;
					}else{
						$like_status = 0;
					}
					$comment['comments_like_status'] = $like_status;
					
					 $reply_data = $songs->curd_read_comment_reply($comment['comments_id']);
					 if(!empty($reply_data))
					 {
						 foreach($reply_data as $reply)
						 {
							$reply_like = $songs->crud_read_reply_likes_unlikes($user_id,$reply['reply_id']);
							if($reply_like > 0)
							{
								$reply_status = 1;
							}else{
								$reply_status = 0;
							}
							$reply['reply_like_status'] = $reply_status;
							
							$user_data = $users->crud_read($reply['user_id']);
							$reply['user_name'] = $user_data[0]['name'];
							if(!empty($user_data[0]['profile_img']))
							{
								$reply['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
							}else{
								$reply['user_profile'] = "";
							}
					
							$comment['reply_on_comment'][] = $reply;
						 }
						
					 }else{
						 $comment['reply_on_comment'] = array();
					 }
					 
					
					$all_comment_arr[] = $comment;
				}
			}
			$returndata['user_comment'] = $user_comment_arr;
			$returndata['all_comment'] = $all_comment_arr;
			
			$returndata['status'] = "success";
			$returndata['message'] = "Comment fetched Successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function favorite_add_remove()
	{
		$returndata = array();
		$songs = new Songs();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$video_id = $this->request->getPost("video_id");
		$favorite_status = $this->request->getPost("favorite_status");
		if(!empty($user_id) && !empty($video_id))
		{
			$check_previous_record = $songs->crud_read_favorite($user_id,$video_id);
			if($check_previous_record > 0)
			{
				$song_id = $songs->crud_update_favorite(array(
					'favorite_status' => $favorite_status
				),$check_previous_record);
				
			}else{
				$song_id = $songs->crud_create_favorite(array(
					'userid' => $user_id,
					'video_id' => $video_id,
					'favorite_status' => $favorite_status
				));
			}
			if($favorite_status == "1")
				{
					$msg = "Added to favorite list!";
				}else{
					$msg = "Removed from favorite list!";
				}
			
			//echo $songs->getLastQuery();
			//die();
			$returndata['status'] = "success";
			$returndata['message'] = $msg;
        	
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function follow_unfollow()
	{
		$returndata = array();
		$songs = new Songs();
		$users = new Users();
		$notifications = new Notifications();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$friend_id = $this->request->getPost("friend_id");
		$follow_status = $this->request->getPost("follow_status");
		if(!empty($user_id) || !empty($friend_id))
		{
			$userdata = $users->crud_read($user_id);
			$userdata1 = $users->crud_read($friend_id);
			$follow_data = $songs->crud_read_follow_status($user_id,$friend_id);
			/* echo $songs->getLastQuery();
			echo "<pre>";
			print_r($follow_data);
			die(); */
			if(count($follow_data) > 0)
			{
				if($follow_data[0]['follow_status'] == "0")
				{
					if($follow_status == "1")
					{
						$song_id = $songs->crud_update_follow(array(
						'follow_status' => $follow_status
						),$follow_data[0]['following_id']);
						$total_follower = $userdata1[0]['total_follower'] + 1;
						$users->crud_update(array('total_follower' => $total_follower),$friend_id);
					}
					
				}else{
					if($follow_status == "0")
					{
						$song_id = $songs->crud_update_follow(array(
						'follow_status' => $follow_status
						),$follow_data[0]['following_id']);
						$total_follower = $userdata1[0]['total_follower'] - 1;
						$users->crud_update(array('total_follower' => $total_follower),$friend_id);
					}
				}
				
				
			}else{
				if($follow_status == "1")
				{
					$song_id = $songs->crud_create_follow(array(
						'userid' => $user_id,
						'friend_id' => $friend_id,
						'follow_status' => $follow_status
					));
				
					$total_follower = $userdata1[0]['total_follower'] + 1;
					$users->crud_update(array('total_follower' => $total_follower),$friend_id);
				}
				
				
			}
			
			if($follow_status == "1")
				{
					$msg = "Follow Successfully!";
					if(empty($userdata1[0]['fcmtoken']))
					{
						$token = "fsOu5DdsJ0LRvZ6PMuY0rF:APA91bFKhmEyWoaqIK3KIK63TlNNHTQ8I-QTkNlR011YjEttsjwMIjhwrTcojh9JQwEvHNj7Tlp0occPzHj1BA4zqv3JV0_P3vHCYcoK3DbgOhn2PXSCCvtncfpgQ5KH3KCneCN--3H5";

					}else{
						$token = $userdata1[0]['fcmtoken'];
					}
					
					$title = "Follow Alert";
					$message = $userdata[0]['name']. " followed You.";
					$this->sendGCM($token,$message,$title);
					$notifications->crud_create(array(
					'friendid' => $user_id,
					'userid' => $friend_id,
					'message' => $message,
					'type' => "Follow",
					'notification_status' => 1,
					'created_at' => $date

					));
					
					
					
				}else{
					
					$msg = "Unfollow Successfully!";
				}
				
			//echo $songs->getLastQuery();
			//die();
			$returndata['status'] = "success";
			$returndata['message'] = $msg;
        	
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_user_videos()
	{
		$returndata = array();
		$songs = new Songs();
		$user_id = $this->request->getPost("user_id");
		//$video_id = $this->request->getPost("video_id");
		if(!empty($user_id))
		{
			$video_arr = array();
			$user_videos = $songs->crud_read_user_video($user_id,'');
			foreach($user_videos as $video)
			{
				if(!empty($video['old_video_id']))
				{
					$video_data = $songs->crud_read_video($video['old_video_id']);
					
					if(!empty($video_data))
					{
						$video['main_video_file'] = base_url()."/writable/uploads/".$video_data[0]['video_file'];
					}else{
						$video['main_video_file'] = base_url()."/writable/uploads/".$video['video_file'];
					}
				}else{
					$video['main_video_file'] = base_url()."/writable/uploads/".$video['video_file'];
				}
				
				$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
				$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
				$video_arr[] = $video;
			}
			$returndata['user_videos'] = $video_arr;
			$returndata['status'] = "success";
			$returndata['message'] = "Videos fetched Successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
 	public function delete_video()
	{
		$returndata = array();
		$songs = new Songs();
		$user_id = $this->request->getPost("user_id");
		$video_id = $this->request->getPost("video_id");
		if(!empty($user_id) && !empty($video_id))
		{
			
			$video_arr = array();
			$user_videos = $songs->crud_read_user_video($user_id,$video_id);
			if(count($user_videos) > 0)
			{
				$songs->crud_delete_user_video($video_id);
				
				$returndata['status'] = "success";
				$returndata['message'] = "Video Deleted Successfully!";
			}else{
				$returndata['status'] = "fail";
				$returndata['message'] = "Video Not Found!";
			}
		
			
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		} 
		$this->apiResponse($returndata);
	}
	
	public function get_lyrics_of_song()
	{
		$returndata = array();
		$songs = new Songs();
		$user_id = $this->request->getPost("user_id");
		$video_id = $this->request->getPost("video_id");
		if(!empty($user_id) && !empty($video_id))
		{
			$video_arr = array();
			$user_videos = $songs->crud_read_user_video($user_id,$video_id);
			/* foreach($user_videos as $video)
			{
				$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
				$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
				$video_arr[] = $video;
			} */
			$returndata['song_lyrics'] = $user_videos[0]['lyrics'];
			$returndata['status'] = "success";
			$returndata['message'] = "Lyrics fetched Successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_like_list()
	{
		$returndata = array();
		$songs = new Songs();
		$user_id = $this->request->getPost("user_id");
		$video_id = $this->request->getPost("video_id");
		if(!empty($user_id) && !empty($video_id))
		{
			$like_arr = array();
			$like_list = $songs->get_user_likes($video_id);
			//echo $songs->getLastQuery();
			/* echo "<pre>";
			print_r($like_list);
			die(); */
			foreach($like_list as $likes)
			{
				$likes['profile_img'] = base_url()."/writable/uploads/".$likes['profile_img'];
				$like_arr[] = $likes;
			}
			$returndata['like_list'] = $like_arr;
			$returndata['status'] = "success";
			$returndata['message'] = "Likes fetched Successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
		
	public function get_video_details()
	{
		$returndata = array();
		$songs = new Songs();
		$users = new Users();
		$user_id = $this->request->getPost("user_id");
		$video_id = $this->request->getPost("video_id");
		if(!empty($user_id) && !empty($video_id))
		{
			$video_arr = array();
			$user_videos = $songs->crud_read_video($video_id);
			foreach($user_videos as $video)
			{
				$user_fav_status = "";
				$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
				if($fav_status >0 )
				{
					$user_fav_status = 1;
				}else{
					$user_fav_status = 0;
				}
				$user_like_status = "";
				$like_status = $songs->crud_read_likes($user_id,$video['video_id']);
				if($like_status >0 )
				{
					$user_like_status = 1;
				}else{
					$user_like_status = 0;
				}
				
				if(!empty($video['main_video_id']))
				{
					$video_data = $songs->crud_read_video($video['main_video_id']);
					
					if(!empty($video_data))
					{
						$video['main_video_file'] = base_url()."/writable/uploads/".$video_data[0]['video_file'];
					}else{
						$video['main_video_file'] = base_url()."/writable/uploads/".$video['video_file'];
					}
				}else{
					$video['main_video_file'] = base_url()."/writable/uploads/".$video['video_file'];
				}
				
				$user_data = $users->crud_read($video['user_id']);
				$video['user_name'] = $user_data[0]['name'];
				$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
				$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
				$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
				$video['user_like_status'] = $user_like_status;
				$video['user_fav_status'] = $user_fav_status;
				$video_arr[] = $video;
			}
			$returndata['video_list'] = $video_arr;
			$returndata['status'] = "success";
			$returndata['message'] = "Videos fetched Successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function get_song_details()
	{
		$returndata = array();
		$songs = new Songs();
		$users = new Users();
		$user_id = $this->request->getPost("user_id");
		$song_id = $this->request->getPost("song_id");
		if(!empty($user_id) && !empty($song_id))
		{
			$song_arr = array();
			$song_details = $songs->crud_read($song_id);
			/* echo "<pre>";
			print_r($song_details);
			die(); */
			foreach($song_details as $song_dtl)
			{
				/* $user_fav_status = "";
				$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
				if($fav_status >0 )
				{
					$user_fav_status = 1;
				}else{
					$user_fav_status = 0;
				} */
				
				$song_dtl['song_track'] = base_url()."/writable/uploads/".$song_dtl['song_track'];
				$song_dtl['song_thumbnail'] = base_url()."/writable/uploads/".$song_dtl['song_thumbnail'];
				//$song_dtl['user_fav_status'] = $user_fav_status;
				$song_arr[] = $song_dtl;
			}
			$returndata['song_details'] = $song_arr;
			$returndata['status'] = "success";
			$returndata['message'] = "Song details fetched successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function upload_audio()
	{
		$returndata = array();
		$songs = new Songs();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$music_cat_id = $this->request->getPost("music_cat_id");
		$old_video_id = $this->request->getPost("old_video_id");
		$song_track = $this->request->getFile("song_track");
		$song_artist = $this->request->getPost("song_artist");
		$song_name = $this->request->getPost("song_name");
		$song_thumb = $this->request->getFile('song_thumb');
		if((!empty($user_id) || !empty($song_track)) && !empty($song_thumb) && !empty($song_name) && !empty($music_cat_id))
		{
			$photopath = "";
			$photopath1 = "";
			if($song_track){
				if($song_track->isValid()){
					$photopath = $song_track->store();
				}
			}
			
			if($song_thumb){
				if($song_thumb->isValid()){
					$photopath1 = $song_thumb->store();
				}
			}
			if(!empty($old_video_id))
			{
			$user_videos = $songs->crud_read_video($old_video_id);
			
			$song_id = $songs->crud_create(array(
							'user_id' => $user_id,
							'music_cat_id' => $music_cat_id,
							'song_name' => $song_name,
							'song_track' => $photopath,
							'song_artist' => $song_artist,
							'song_status' => 1,
							'song_thumbnail' => $photopath1,
							'old_video_id' => $old_video_id,
							'main_video_id' => $user_videos[0]['main_video_id'],
							'created_at' => $date
						));
				$total_sing = $user_videos[0]['total_sing'] + 1;
				$songs->crud_update_video(array(
					'total_sing' => $total_sing
					),$old_video_id);	
			}else{
				$song_id = $songs->crud_create(array(
							'user_id' => $user_id,
							'music_cat_id' => $music_cat_id,
							'song_name' => $song_name,
							'song_track' => $photopath,
							'song_artist' => $song_artist,
							'song_status' => 1,
							'song_thumbnail' => $photopath1,
							'created_at' => $date
						));
						
				$songs->crud_update_video(array(
					'old_video_id' => $song_id,'main_video_id' => $song_id
					),$song_id);
			}
			
			$returndata['status'] = "success";
        	$returndata['message'] = "Song uploaded successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
 	public function get_notification_list()
	{
		$returndata = array();
		$users = new Users();
		$songs = new Songs();
		$notifications = new Notifications();
		$user_id = $this->request->getPost("user_id");
		$last_id = $this->request->getPost("last_id");
		if((!empty($user_id) || !empty($last_id)))
		{
			$notification_list = $notifications->crud_filter_notification($user_id,$last_id);
			/* echo $notifications->getLastQuery();
			echo "<pre>";
			print_r($notification_list);
			die(); */
			$msg_arr = array();
			foreach($notification_list as $notification)
			{
				$user_data = $users->crud_read($notification['friendid']);
				$notification['name'] = $user_data[0]['name'];
				$notification['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
				if($notification['type'] != 'Follow')
				{
					$video_arr =array();
					$user_videos = $songs->crud_read_video($notification['videoid']);
					foreach($user_videos as $video)
					{
						$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
						$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
						$notification['video_details'] = $video;
						
						
					}
					
					
				}else{
					//$notification['video_details'] = array();
				}
				
				$user_status = $songs->crud_read_follow($notification['userid'],$notification['friendid']);
					if($user_status > 0)
					{
						$user_follow_status = 1;
					}else{
						$user_follow_status = 0;
					}
					$friend_status = $songs->crud_read_follow($notification['friendid'],$notification['userid']);
					if($friend_status > 0)
					{
						$friend_follow_status = 1;
					}else{
						$friend_follow_status = 0;
					}
					
					$notification['user_follow_status'] = $user_follow_status; 
					$notification['friend_follow_status'] = $friend_follow_status; 
					$user_likes = $songs->crud_read_post_likes($notification['videoid']);
					$like_arr = array();
					foreach($user_likes as $likes)
					{
						$user_data1 = $users->crud_read($likes['user_id']);
						$likes['name'] = $user_data1[0]['name'];
						$likes['user_profile'] = base_url()."/writable/uploads/".$user_data1[0]['profile_img'];
						$like_arr[] = $likes;
					}
					
				
					$notification['user_likes'] = $like_arr;
					$msg_arr[] = $notification;
					
			}
			$returndata['notification_list'] = $msg_arr;
			$returndata['status'] = "success";
        	$returndata['message'] = "Notification fetched successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function may_be_later()
	{
		$returndata = array();
		$users = new Users();
		$songs = new Songs();
		$notifications = new Notifications();
		$user_id = $this->request->getPost("user_id");
		$notification_id = $this->request->getPost("notification_id");
		$status = $this->request->getPost("status");
		if(!empty($user_id) || !empty($notification_id))
		{
			$notification_list = $notifications->crud_read($notification_id);
			if(count($notification_list) > 0 )
			{
				$notifications->crud_update(array('maybelater' => $status),$notification_id);
				
				$returndata['status'] = "success";
				$returndata['message'] = "Notification updated successfully!";
			}else{
				$returndata['status'] = "error";
				$returndata['message'] = "Notification not found!";
			}
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function video_views()
	{
		$returndata = array();
		$songs = new Songs();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$video_id = $this->request->getPost("video_id");
		if((!empty($user_id) || !empty($video_id)))
		{
			$video_data = $songs->crud_read_video($video_id);
			if(!empty($video_data))
			{
				$total_views = $video_data[0]['total_views'] + 1;
				$songs->crud_update_video(array('total_views' => $total_views
				),$video_id);
			}
			
			
			$returndata['status'] = "success";
        	$returndata['message'] = "Video Views added successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function add_user_music_choice()
	{
		$returndata = array();
		$songs = new Songs();
		$date = date('Y-m-d H:i:s');
		$user_id = $this->request->getPost("user_id");
		$music_category = $this->request->getPost("music_category");
		
		if(!empty($user_id) && !empty($music_category))
		{
			$music_category_list = explode(",",$music_category);
			/* echo "<pre>";
			print_r($music_category_list);
			die(); */
			$songs->delete_user_music_choice($user_id);
			foreach($music_category_list as $category)
			{
				$choice_id = $songs->add_user_music_choice(array(
							'userid' => $user_id,
							'music_cat_id' => $category,
							'createat' => $date
						));
			}
			$returndata['status'] = "success";
        	$returndata['message'] = "User music choice added successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	
	public function search_song()
	{
		$returndata = array();
		$songs = new Songs();
		$users = new Users();
		$block_unblock = new Block_unblock();
		$user_id = $this->request->getPost("user_id");
		$txt = $this->request->getPost("txt");
		if(!empty($user_id) || !empty($txt))
		{	
			$video_arr = array();
			$search_result = $songs->crud_filter_song($txt);
			/* echo $songs->getLastQuery();
			print_r($search_result);
			die(); */
			foreach($search_result as $video)
			{
				$user_fav_status = "";
				$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
				if($fav_status >0 )
				{
					$user_fav_status = 1;
				}else{
					$user_fav_status = 0;
				}
				
				$user_like_status = "";
				$like_status = $songs->crud_read_likes($user_id,$video['video_id']);
				if($like_status >0 )
				{
					$user_like_status = 1;
				}else{
					$user_like_status = 0;
				}
				
				$user_follow_status = "";
				$follow_status = $songs->crud_read_follow($user_id,$video['user_id']);
				if($follow_status >0 )
				{
					$user_follow_status = 1;
				}else{
					$user_follow_status = 0;
				}
				
				$block_list = $block_unblock->crud_read($user_id,$video['user_id']);
				if(!empty($block_list)){
					if($block_list[0]['block_status'] == 1){
						$user_block_status = 1;
					}else{
						$user_block_status = 0;
					}
				}else{
						$user_block_status = 0;
				}
			
				$block_list1 = $block_unblock->crud_read($video['user_id'], $user_id);
				if(!empty($block_list1)){
					if($block_list1[0]['block_status'] == 1){
						$friend_block_status = 1;
					}else{
						$friend_block_status = 0;
					}
				}else{
						$friend_block_status = 0;
				}
				
				if($user_block_status ==0 && $friend_block_status ==0)
				{
				
					$user_data = $users->crud_read($video['user_id']);
					$video['user_name'] = $user_data[0]['name'];
					$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
					$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
					$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
					$video['user_fav_status'] = $user_fav_status;
					$video['user_like_status'] = $user_like_status;
					$video['user_follow_status'] = $user_follow_status;
					$video_arr[] = $video;
				}else{
					continue;
				}
			}
			/* echo $songs->getLastQuery();
			echo "<pre>";
			print_r($video_arr);
			die(); */
			$returndata['search_result'] = $video_arr;
			$returndata['status'] = "success";
        	$returndata['message'] = "Search Result";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
		/* ----send notification ----- */
	
	public function sendGCM($token,$msg,$title) {
		//$message = "You got a new match!";
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array (
        'to' => $token,
        'notification' => array (
                "body" => $msg,
                "title" => $title,
                "icon" => "myicon"
        )
		);
		$fields = json_encode ( $fields );
		$firebase_key = "AAAAlQfrVzo:APA91bEjo5rMYM01JFjyqGNuIelC_TC5eiTRtAQW3Ar_2rTYDeNXAc4T2pTw0KTs_ncwv8zGASVlt-i5Sqt-jf5nhOzYMID4g8tz0SoeW5P64bsdE5hikyFyIxha6H-kyIDtn5UtuAjx";
		$headers = array (
				'Authorization: key=' . $firebase_key,
				'Content-Type: application/json'
		);

		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		$result = curl_exec ( $ch );
		curl_close ( $ch );
	}
	
	/*-------------Explore Screen API---------------*/

	public function explore_screen()
	{
		$returndata = array();
		$data =array();
		$songs = new Songs();
		$users = new Users();
		$block_unblock = new Block_unblock();
		$user_id = $this->request->getPost("user_id");
		$userdata = $users->crud_read($user_id);
		if(!empty($user_id)){
			$min_range = 0;
			$max_range = 5;
			$recomended_video_arr = array();
				/* $music_choice = $songs->curd_read_music_choice($user_id);
				if(count($music_choice) > 0)
				{
					$choice_arr = array();
					foreach($music_choice as $user_choice)
					{
						$choice_arr[]  = $user_choice['music_cat_id'];
					}
					
					$userchoice = implode(",",$choice_arr);
					$user_videos = $songs->crud_read_recomended_video($min_range,$max_range);
				} */
				$recomended_videos = $songs->crud_read_recomended_video($min_range,$max_range);
				
					foreach($recomended_videos as $video)
					{
						$user_fav_status = "";
						$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
						if($fav_status >0 )
						{
							$user_fav_status = 1;
						}else{
							$user_fav_status = 0;
						}
						
						$user_like_status = "";
						$like_status = $songs->crud_read_likes($user_id,$video['video_id']);
						if($like_status >0 )
						{
							$user_like_status = 1;
						}else{
							$user_like_status = 0;
						}
						
						$user_follow_status = "";
						$follow_status = $songs->crud_read_follow($user_id,$video['user_id']);
						if($follow_status >0 )
						{
							$user_follow_status = 1;
						}else{
							$user_follow_status = 0;
						}
						
						$block_list = $block_unblock->crud_read($user_id,$video['user_id']);
						if(!empty($block_list)){
							if($block_list[0]['block_status'] == 1){
								$user_block_status = 1;
							}else{
								$user_block_status = 0;
							}
						}else{
								$user_block_status = 0;
						}
					
						$block_list1 = $block_unblock->crud_read($video['user_id'], $user_id);
						if(!empty($block_list1)){
							if($block_list1[0]['block_status'] == 1){
								$friend_block_status = 1;
							}else{
								$friend_block_status = 0;
							}
						}else{
								$friend_block_status = 0;
						}
						
						if($user_block_status ==0 && $friend_block_status ==0)
						{
							$user_data = $users->crud_read($video['user_id']);
							$video['user_name'] = $user_data[0]['name'];
							$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
							$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
							$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
							$video['user_fav_status'] = $user_fav_status;
							$video['user_like_status'] = $user_like_status;
							$video['user_follow_status'] = $user_follow_status;
							$recomended_video_arr[] = $video;
						}else{
							continue;
						}
					}
								
				$popular_video_arr = array();
				$popular_videos = $songs->crud_read_popular_video($min_range,$max_range);
				/* echo $songs->getLastQuery();
				print_r($popular_videos);
				die();	 */
				foreach($popular_videos as $video)
				{
					$user_fav_status = "";
					$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
					if($fav_status >0 )
					{
						$user_fav_status = 1;
					}else{
						$user_fav_status = 0;
					}
					
					$user_like_status = "";
					$like_status = $songs->crud_read_likes($user_id,$video['video_id']);
					if($like_status >0 )
					{
						$user_like_status = 1;
					}else{
						$user_like_status = 0;
					}
					
					$user_follow_status = "";
					$follow_status = $songs->crud_read_follow($user_id,$video['user_id']);
					if($follow_status >0 )
					{
						$user_follow_status = 1;
					}else{
						$user_follow_status = 0;
					}
					
					
					$block_list = $block_unblock->crud_read($user_id,$video['user_id']);
						if(!empty($block_list)){
							if($block_list[0]['block_status'] == 1){
								$user_block_status = 1;
							}else{
								$user_block_status = 0;
							}
						}else{
								$user_block_status = 0;
						}
					
						$block_list1 = $block_unblock->crud_read($video['user_id'], $user_id);
						if(!empty($block_list1)){
							if($block_list1[0]['block_status'] == 1){
								$friend_block_status = 1;
							}else{
								$friend_block_status = 0;
							}
						}else{
								$friend_block_status = 0;
						}
						
						if($user_block_status ==0 && $friend_block_status ==0)
						{
							
							$user_data = $users->crud_read($video['user_id']);
							$video['user_name'] = $user_data[0]['name'];
							$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
							$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
							$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
							$video['user_fav_status'] = $user_fav_status;
							$video['user_like_status'] = $user_like_status;
							$video['user_follow_status'] = $user_follow_status;
							$popular_video_arr[] = $video;
						}else{
							continue;
						}
				}
			
				
				$trending_video_arr = array();
				$trending_videos = $songs->crud_read_trending_video($min_range,$max_range);
				foreach($trending_videos as $video)
				{
				$user_fav_status = "";
				$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
				if($fav_status >0 )
				{
					$user_fav_status = 1;
				}else{
					$user_fav_status = 0;
				}
				
				$user_like_status = "";
				$like_status = $songs->crud_read_likes($user_id,$video['video_id']);
				if($like_status >0 )
				{
					$user_like_status = 1;
				}else{
					$user_like_status = 0;
				}
				
				$user_follow_status = "";
				$follow_status = $songs->crud_read_follow($user_id,$video['user_id']);
				if($follow_status >0 )
				{
					$user_follow_status = 1;
				}else{
					$user_follow_status = 0;
				}
				
				$block_list = $block_unblock->crud_read($user_id,$video['user_id']);
				if(!empty($block_list)){
					if($block_list[0]['block_status'] == 1){
						$user_block_status = 1;
					}else{
						$user_block_status = 0;
					}
				}else{
						$user_block_status = 0;
				}
			
				$block_list1 = $block_unblock->crud_read($video['user_id'], $user_id);
				if(!empty($block_list1)){
					if($block_list1[0]['block_status'] == 1){
						$friend_block_status = 1;
					}else{
						$friend_block_status = 0;
					}
				}else{
						$friend_block_status = 0;
				}
				
				if($user_block_status ==0 && $friend_block_status ==0)
				{
				
					$user_data = $users->crud_read($video['user_id']);
					$video['user_name'] = $user_data[0]['name'];
					$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
					$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
					$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
					$video['user_fav_status'] = $user_fav_status;
					$video['user_like_status'] = $user_like_status;
					$video['user_follow_status'] = $user_follow_status;
					$trending_video_arr[] = $video;
				}else{
					continue;
				}
				}
				
				
				$top_artist_video_arr = array();
				$top_artists = $users->crud_read_top_artist($min_range,$max_range);
				if(!empty($top_artists))
				{
					foreach($top_artists as $artist)
					{
						$top_artist_videos = $songs->crud_read_top_artist_video($artist['user_id']);
						
						foreach($top_artist_videos as $video)
						{
						$user_fav_status = "";
						$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
						if($fav_status >0 )
						{
							$user_fav_status = 1;
						}else{
							$user_fav_status = 0;
						}
						
						$user_like_status = "";
						$like_status = $songs->crud_read_likes($user_id,$video['video_id']);
						if($like_status >0 )
						{
							$user_like_status = 1;
						}else{
							$user_like_status = 0;
						}
						
						$user_follow_status = "";
						$follow_status = $songs->crud_read_follow($user_id,$video['user_id']);
						if($follow_status >0 )
						{
							$user_follow_status = 1;
						}else{
							$user_follow_status = 0;
						}
						
						
						$block_list = $block_unblock->crud_read($user_id,$video['user_id']);
						if(!empty($block_list)){
							if($block_list[0]['block_status'] == 1){
								$user_block_status = 1;
							}else{
								$user_block_status = 0;
							}
						}else{
								$user_block_status = 0;
						}
					
						$block_list1 = $block_unblock->crud_read($video['user_id'], $user_id);
						if(!empty($block_list1)){
							if($block_list1[0]['block_status'] == 1){
								$friend_block_status = 1;
							}else{
								$friend_block_status = 0;
							}
						}else{
								$friend_block_status = 0;
						}
						
						if($user_block_status ==0 && $friend_block_status ==0)
						{
					
							$user_data = $users->crud_read($video['user_id']);
							$video['user_name'] = $user_data[0]['name'];
							$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
							$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
							$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
							$video['user_fav_status'] = $user_fav_status;
							$video['user_like_status'] = $user_like_status;
							$video['user_follow_status'] = $user_follow_status;
							$top_artist_video_arr[] = $video;
						}else{
							continue;
						}
						}
					}
				}
				
				$top_singer_video_arr = array();
				$top_singers = $users->crud_read_top_singer($min_range,$max_range);
				if(!empty($top_singers))
				{
					foreach($top_singers as $singer)
					{
						$top_singer_videos = $songs->crud_read_top_singer_video($singer['user_id']);
						
						foreach($top_singer_videos as $video)
						{
						$user_fav_status = "";
						$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
						if($fav_status >0 )
						{
							$user_fav_status = 1;
						}else{
							$user_fav_status = 0;
						}
						
						$user_like_status = "";
						$like_status = $songs->crud_read_likes($user_id,$video['video_id']);
						if($like_status >0 )
						{
							$user_like_status = 1;
						}else{
							$user_like_status = 0;
						}
						
						$user_follow_status = "";
						$follow_status = $songs->crud_read_follow($user_id,$video['user_id']);
						if($follow_status >0 )
						{
							$user_follow_status = 1;
						}else{
							$user_follow_status = 0;
						}
						
						$block_list = $block_unblock->crud_read($user_id,$video['user_id']);
						if(!empty($block_list)){
							if($block_list[0]['block_status'] == 1){
								$user_block_status = 1;
							}else{
								$user_block_status = 0;
							}
						}else{
								$user_block_status = 0;
						}
					
						$block_list1 = $block_unblock->crud_read($video['user_id'], $user_id);
						if(!empty($block_list1)){
							if($block_list1[0]['block_status'] == 1){
								$friend_block_status = 1;
							}else{
								$friend_block_status = 0;
							}
						}else{
								$friend_block_status = 0;
						}
						
						if($user_block_status ==0 && $friend_block_status ==0)
						{
						
							$user_data = $users->crud_read($video['user_id']);
							$video['user_name'] = $user_data[0]['name'];
							$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
							$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
							$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
							$video['user_fav_status'] = $user_fav_status;
							$video['user_like_status'] = $user_like_status;
							$video['user_follow_status'] = $user_follow_status;
							$top_singer_video_arr[] = $video;
						}else{
							continue;
						}
						}
					}
				}	
			$returndata['recomended_videos'] = $recomended_video_arr;
			$returndata['popular_videos'] = $popular_video_arr;
			$returndata['trending_videos'] = $trending_video_arr;
			$returndata['top_artist_videos'] = $top_artist_video_arr;
			$returndata['top_singer_videos'] = $top_singer_video_arr;
		}
		else{
			$returndata['status'] = "error";
			$returndata['message'] = "Please enter valid parameters";
		}
		return $this->respond($returndata,200);
	}
	
	/*-------------Song List API---------------*/

	public function get_song_list()
	{
		$returndata = array();
		$data =array();
		$songs = new Songs();
		$users = new Users();
		$block_unblock = new Block_unblock();
		$user_id = $this->request->getPost("user_id");
		$type = $this->request->getPost("type");
		$max_range = $this->request->getPost("max_range");
		$min_range = $this->request->getPost("min_range");
		if(!empty($user_id)){
			$video_arr = array();
			$$user_videos = array();
			if($type == "recomended")
			{
				/* $music_choice = $songs->curd_read_music_choice($user_id);
				if(count($music_choice) > 0)
				{
					$choice_arr = array();
					foreach($music_choice as $user_choice)
					{
						$choice_arr[]  = $user_choice['music_cat_id'];
					}
					
					$userchoice = implode(",",$choice_arr);
					$user_videos = $songs->crud_read_recomended_video($min_range,$max_range);
				} */
				$user_videos = $songs->crud_read_recomended_video($min_range,$max_range);
				//$user_videos = $songs->crud_read_user_likes();
				/* echo $songs->getLastQuery();
				echo "<pre>";
				print_r($user_videos);
				die(); */
			
			}else if($type == "popular")
			{
				$user_videos = $songs->crud_read_popular_video($min_range,$max_range);
				
			}else if($type == "trending")
			{
				$user_videos = $songs->crud_read_trending_video($min_range,$max_range);
				
			}else if($type == "top_artist")
			{
				$user_videos = array();
				$top_artists = $users->crud_read_top_artist($min_range,$max_range);
				if(!empty($top_artists))
				{
					foreach($top_artists as $artist)
					{
						$artist_video = $songs->crud_read_top_artist_video($artist['user_id']);
						$user_videos[] = $artist_video[0];
					}
				}
				
				
			}else if($type == "top_singer")
			{
				$user_videos = array();
				$top_singers = $users->crud_read_top_singer($min_range,$max_range);
				if(!empty($top_singers))
				{
					foreach($top_singers as $singer)
					{
						$singer_video = $songs->crud_read_top_singer_video($singer['user_id']);
						$user_videos[] = $singer_video[0];
					}
				}
				
			}
			/* print_r($user_videos);
			die(); */
			if(!empty($user_videos))
			{
				foreach($user_videos as $video)
				{
					$user_fav_status = "";
					$fav_status = $songs->crud_read_favorite($user_id,$video['video_id']);
					if($fav_status >0 )
					{
						$user_fav_status = 1;
					}else{
						$user_fav_status = 0;
					}
					
					$user_like_status = "";
					$like_status = $songs->crud_read_likes($user_id,$video['video_id']);
					if($like_status >0 )
					{
						$user_like_status = 1;
					}else{
						$user_like_status = 0;
					}
					
					$user_follow_status = "";
					$follow_status = $songs->crud_read_follow($user_id,$video['user_id']);
					if($follow_status >0 )
					{
						$user_follow_status = 1;
					}else{
						$user_follow_status = 0;
					}
					
					$block_list = $block_unblock->crud_read($user_id,$video['user_id']);
					if(!empty($block_list)){
						if($block_list[0]['block_status'] == 1){
							$user_block_status = 1;
						}else{
							$user_block_status = 0;
						}
					}else{
							$user_block_status = 0;
					}
				
					$block_list1 = $block_unblock->crud_read($video['user_id'], $user_id);
					if(!empty($block_list1)){
						if($block_list1[0]['block_status'] == 1){
							$friend_block_status = 1;
						}else{
							$friend_block_status = 0;
						}
					}else{
							$friend_block_status = 0;
					}
					
					if($user_block_status ==0 && $friend_block_status ==0){
							
						$user_data = $users->crud_read($video['user_id']);
						$video['user_name'] = $user_data[0]['name'];
						$video['user_profile'] = base_url()."/writable/uploads/".$user_data[0]['profile_img'];
						$video['video_file'] = base_url()."/writable/uploads/".$video['video_file'];
						$video['video_thumb'] = base_url()."/writable/uploads/".$video['video_thumb'];
						$video['user_fav_status'] = $user_fav_status;
						$video['user_like_status'] = $user_like_status;
						$video['user_follow_status'] = $user_follow_status;
						$video_arr[] = $video;
					}else{
						continue;
					}
				}
			}
			$returndata['status'] = "success";
			$returndata['video_list'] = $video_arr;
			$returndata['message'] = "Video List";
		}
		else{
			$returndata['status'] = "error";
			$returndata['message'] = "Please enter valid parameters";
		}
		return $this->respond($returndata,200);
	}
	
	public function get_sound_effects()
	{
		$returndata = array();
		$songs = new Songs();
		$user_id = $this->request->getPost("user_id");
		if(!empty($user_id))
		{
			$sound_arr = array();
			$sounds = $songs->crud_read_sound_effects();
			foreach($sounds as $sound)
			{
				$sound['effect_url'] = base_url()."/writable/uploads/".$sound['effect_url'];
				$sound_arr[] = $sound;
			}
			$returndata['sound_list'] = $sound_arr;
			$returndata['status'] = "success";
			$returndata['message'] = "Sound list fetched Successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
	
	public function submit_query()
	{
		$returndata = array();
		$contact = new Contact();
		$user_id = $this->request->getPost("user_id");
		$msg = $this->request->getPost("msg");
		$datetime = date('Y-m-d');
		if(!empty($user_id))
		{
			$contact_id = $contact->crud_create(array('userid'=>$user_id,'message'=>$msg,'created_at'=>$datetime));
			
			$returndata['status'] = "success";
			$returndata['message'] = "Query submitted Successfully!";
		}
		else
		{
			$returndata['status'] = "fail";
        	$returndata['message'] = "Please provide required fields.";
		}
		$this->apiResponse($returndata);
	}
}